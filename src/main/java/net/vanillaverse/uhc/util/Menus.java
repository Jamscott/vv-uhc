package net.vanillaverse.uhc.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.function.Consumer;

public class Menus implements Listener {

    private static HashMap<Inventory, Menu> activeMenus = new HashMap<>();

    public static class Menu {

        private HashMap<Integer, MenuItem> items = new HashMap<>();
        private String name; private int size;

        public Menu(String name, int size) {
            this.name = name;
            this.size = size;
        }

        public MenuItem getItem(Integer integer) {
            if (items.containsKey(integer)) {
                return items.get(integer);
            } else {
                return null;
            }
        }

        public void setItem(Integer integer, MenuItem menuItem) {
            items.put(integer, menuItem);

            if (activeMenus.containsValue(this)) {
                activeMenus.keySet().stream().filter(inventory -> activeMenus.get(inventory) == this).forEach(inventory -> inventory.setItem(integer, menuItem.getItemStack()));
            }
        }

        public void clearItems() {
            items.clear();

            if (activeMenus.containsValue(this)) {
                activeMenus.keySet().stream().filter(inventory -> activeMenus.get(inventory) == this).forEach(Inventory::clear);
            }
        }

        public void removeItem(Integer integer) {
            items.remove(integer);

            activeMenus.keySet().stream().filter(inventory -> activeMenus.get(inventory) == this).forEach(inventory -> inventory.remove(inventory.getItem(integer)));
        }

        public String getName() {
            return name;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        private Inventory createMenu() {
            Inventory inventory = Bukkit.createInventory(null, getSize(), getName());

            for (int item : items.keySet()) {
                inventory.setItem(item, items.get(item).getItemStack());
            }

            return inventory;
        }

        public void showMenu(Player player) {

            Inventory inventory = createMenu();

            player.openInventory(inventory);
            activeMenus.put(inventory, this);
        }

    }

    public static class MenuItem {

        private ItemStack itemStack; private Consumer<InventoryClickEvent> consumer;

        public MenuItem(ItemStack itemStack, Consumer<InventoryClickEvent> consumer) {
            this.itemStack = itemStack;
            this.consumer = consumer;
        }

        public ItemStack getItemStack() {
            return itemStack;
        }

        public Consumer<InventoryClickEvent> getConsumer() {
            return consumer;
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (activeMenus.containsKey(event.getClickedInventory()) && activeMenus.get(event.getClickedInventory()).getItem(event.getSlot()) != null) {
            Inventory inventory = event.getClickedInventory();
            activeMenus.get(inventory).getItem(event.getSlot()).getConsumer().accept(event);

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (activeMenus.containsKey(event.getInventory())) {
            if (event.getViewers().size() == 1) {
                activeMenus.remove(event.getInventory());
            }

        }
    }

}
