/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  net.minecraft.server.v1_9_R1.ItemStack
 *  net.minecraft.server.v1_9_R1.NBTBase
 *  net.minecraft.server.v1_9_R1.NBTTagCompound
 *  net.minecraft.server.v1_9_R1.NBTTagList
 *  org.bukkit.Material
 *  org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemFlag
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.SkullMeta
 */
package net.vanillaverse.uhc.util;

import net.minecraft.server.v1_9_R1.ItemStack;
import net.minecraft.server.v1_9_R1.NBTBase;
import net.minecraft.server.v1_9_R1.NBTTagCompound;
import net.minecraft.server.v1_9_R1.NBTTagList;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Random;

public class ItemUtils {
    public static /* varargs */ org.bukkit.inventory.ItemStack makeItem(Material material, String name, String ... lore) {
        org.bukkit.inventory.ItemStack item = new org.bukkit.inventory.ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(Arrays.asList(lore));
        item.setItemMeta(itemMeta);
        return item;
    }

    public static /* varargs */ org.bukkit.inventory.ItemStack makeHead(String username, String name, String ... lore) {
        org.bukkit.inventory.ItemStack item = new org.bukkit.inventory.ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta itemMeta = (SkullMeta)item.getItemMeta();
        itemMeta.setOwner(username);
        itemMeta.setDisplayName(name);
        itemMeta.setLore(Arrays.asList(lore));
        item.setItemMeta((ItemMeta)itemMeta);
        return item;
    }

    public static /* varargs */ org.bukkit.inventory.ItemStack makeItem(Material material, int count, String name, String ... lore) {
        org.bukkit.inventory.ItemStack item = new org.bukkit.inventory.ItemStack(material, count);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(Arrays.asList(lore));
        item.setItemMeta(itemMeta);
        return item;
    }

    public static /* varargs */ org.bukkit.inventory.ItemStack makeItem(Material material, int count, int data, String name, String ... lore) {
        org.bukkit.inventory.ItemStack item = new org.bukkit.inventory.ItemStack(material, count, (short)data);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(Arrays.asList(lore));
        item.setItemMeta(itemMeta);
        return item;
    }

    public static org.bukkit.inventory.ItemStack emptyEnchant(org.bukkit.inventory.ItemStack item) {
        ItemStack nmsStack = CraftItemStack.asNMSCopy((org.bukkit.inventory.ItemStack)item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) {
            tag = nmsStack.getTag();
        }
        NBTTagList ench = new NBTTagList();
        tag.set("ench", (NBTBase)ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror((ItemStack)nmsStack);
    }

    public static org.bukkit.inventory.ItemStack enchant(org.bukkit.inventory.ItemStack itemStack) {
        itemStack.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        return itemStack;
    }

    public static org.bukkit.inventory.ItemStack hideAttrb(org.bukkit.inventory.ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS});
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static org.bukkit.inventory.ItemStack random(org.bukkit.inventory.ItemStack itemStack) {
        ItemStack nmsStack = CraftItemStack.asNMSCopy((org.bukkit.inventory.ItemStack)itemStack);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) {
            tag = nmsStack.getTag();
        }
        NBTTagList ench = new NBTTagList();
        tag.setString("randomAttr", "" + new Random().nextDouble() + "");
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror((ItemStack)nmsStack);
    }

    public static org.bukkit.inventory.ItemStack removeRandom(org.bukkit.inventory.ItemStack itemStack) {
        ItemStack nmsStack = CraftItemStack.asNMSCopy((org.bukkit.inventory.ItemStack)itemStack);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) {
            tag = nmsStack.getTag();
        }
        if (tag.hasKey("randomAttr")) {
            return new org.bukkit.inventory.ItemStack(itemStack.getType(), itemStack.getAmount(), itemStack.getDurability());
        }
        return itemStack;
    }
}

