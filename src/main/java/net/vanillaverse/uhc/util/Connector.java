package net.vanillaverse.uhc.util;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.vanillaverse.uhc.UHC;
import org.bukkit.entity.Player;

public class Connector {

    public static void connect(Player p, String server){
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(UHC.INSTANCE  , "BungeeCord", out.toByteArray());
    }



}
