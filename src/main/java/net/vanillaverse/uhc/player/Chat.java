package net.vanillaverse.uhc.player;

import net.minecraft.server.v1_9_R1.IChatBaseComponent;
import net.minecraft.server.v1_9_R1.Packet;
import net.minecraft.server.v1_9_R1.PacketPlayOutChat;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.game.UhcGameSettings;
import net.vanillaverse.uhc.party.Party;
import net.vanillaverse.uhc.party.PartyManager;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class Chat {

    public static final String BOLD = "" + ChatColor.BOLD;

    public static void announceDecrease() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "The world border will shrink to " + UWorldManager.FINAL_SIZE + " blocks over " + (UWorldManager.DECREASE_TIME / 60) + " minutes!");
    }

    public static void sendNoSpaceInTeam(Player player, UTeam uTeam) {
        player.sendMessage(ChatColor.RED + "There is no space left in the " + uTeam.getChatColor() + BOLD + uTeam.toString() + ChatColor.RED + "  team!");
    }

    public static void switchTeam(Player player, UTeam uTeam) {
        player.sendMessage(ChatColor.YELLOW + "You joined the " + uTeam.getChatColor() + BOLD + uTeam.getName() + ChatColor.YELLOW + " team!");
        sendTitle(player, " ", ChatColor.YELLOW + "You joined the " + uTeam.getChatColor() + BOLD + uTeam.getName() + ChatColor.YELLOW + " team!");
    }

    public static void sendSetTeams(CommandSender commandSender, int i) {
        commandSender.sendMessage(ChatColor.GOLD + "Set team number to " + BOLD + i + ChatColor.GOLD + "!");
    }

    public static void sendChangeSlots(CommandSender commandSender, int i) {
        commandSender.sendMessage(ChatColor.GOLD + "Set player slots to " + BOLD + i + ChatColor.GOLD + "!");
    }

    public static void sendNotEnoughPlayers(CommandSender commandSender) {
        commandSender.sendMessage(ChatColor.RED + "There need to be 2 players for the game to start!");
    }
    public static void sendAlreadyStarted(CommandSender commandSender) {
        commandSender.sendMessage(ChatColor.RED + "The game has already started!");
    }

    public static void sendPlayerSpectate(Player player, Player player2) {
        sendInActionbar(ChatColor.YELLOW + ChatColor.BOLD.toString() + "You are now spectating: " + player2.getName(), player);
    }

    public static void sendNotStarted(CommandSender commandSender) {
        commandSender.sendMessage(ChatColor.RED + "The game has not yet started!");
    }

    public static void gameStarting(int seconds) {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "The game is starting in " + BOLD + seconds + " seconds" + ChatColor.YELLOW + "!");
    }
    public static void gameDistr(int seconds) {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Team distribution is starting in " + BOLD + seconds + " seconds" + ChatColor.YELLOW + "!");
    }


    public static void startCancel() {
        Bukkit.broadcastMessage(ChatColor.RED + BOLD + "Game start has been cancelled!");
    }

    public static void announceReward(String reward, int step) {
        String s = step == 1 ? "1st" : step == 2 ? "2nd" : "3rd";
        Bukkit.broadcastMessage(ChatColor.YELLOW + "The " + s + " reward is " + reward + ChatColor.YELLOW + "!");
    }

    public static void announceRewards() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "It's time to announce the " + ChatColor.GOLD + Chat.BOLD + "Rewards" + ChatColor.YELLOW + "!");
    }

    public static void announceTeamDis() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "Distributing players... ");
        Bukkit.getOnlinePlayers().forEach(p -> Chat.sendTitle(p , ChatColor.AQUA + ChatColor.BOLD.toString() + "Ultra Hardcore", ChatColor.GRAY + "Distributing players"));
    }

    public static void partyBarAlert(){
        if(UGameManager.isTeamed()){
            for(Player player : Bukkit.getOnlinePlayers()){
                if(PartyManager.getParty(player) != null){
                    Chat.sendInActionbar(ChatColor.WHITE + ChatColor.BOLD.toString() + PartyManager.getParty(player).getLeader().getName() + "'s Party    " + ChatColor.GREEN + ChatColor.BOLD.toString() + PartyManager.getParty(player).getPlayers().size() + ChatColor.WHITE + ChatColor.BOLD.toString() + "/" + ChatColor.GREEN + ChatColor.BOLD.toString() + UGameManager.getTeamSizeLimit(), player);
                }else{
                    Chat.sendInActionbar(ChatColor.WHITE + ChatColor.BOLD.toString() + "To team with friends type " + ChatColor.GREEN + ChatColor.BOLD.toString() + "/party" + ChatColor.WHITE + ChatColor.BOLD.toString() + "!", player);
                }
            }
        }
    }


    public static void sendLeftTeam(CommandSender commandSender) {
        commandSender.sendMessage(ChatColor.YELLOW + "You left your current team!");
    }

    public static void winner(UPlayer pwinner) {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.broadcastMessage(ChatColor.GREEN + BOLD + pwinner.getPlayer().getName() + " has won the game!");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.getOnlinePlayers().forEach(p -> {
            sendTitle(pwinner.getPlayer(), ChatColor.YELLOW + BOLD + pwinner.getPlayer().getName() , ChatColor.AQUA + " Is the CHAMPION!");
        });

    }

    public static void winner(UTeam twinner) {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.broadcastMessage(ChatColor.GREEN + BOLD + "Team " + twinner.getChatColor() + BOLD + twinner.getName() + ChatColor.GREEN + BOLD + " has won the game!");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.getOnlinePlayers().forEach(p -> {
            sendTitle(p, twinner.getChatColor() + twinner.getChatColor().toString() + BOLD + "Team " +  twinner.getName() , ChatColor.AQUA + "Are the CHAMPIONS!");
        });
    }

    public static void sendChangeTournament(CommandSender commandSender, boolean target) {
        if (target) {
            commandSender.sendMessage(ChatColor.GREEN + "Enabled tournament mode!");
        } else {
            commandSender.sendMessage(ChatColor.GOLD + "Disabled tournament mode!");
        }
    }

    public static void announceGameStart(){
        Bukkit.getOnlinePlayers().forEach(p -> Chat.sendTitle(p, ChatColor.AQUA + ChatColor.BOLD.toString() + "Ultra Hardcore", ChatColor.GRAY + "The game has begun!"));
    }

    public static void announceGraceEnd(){
        Bukkit.getOnlinePlayers().forEach(p -> {
            p.playSound(p.getLocation(), Sound.ENTITY_WITHER_SPAWN, 100, 10);
            Chat.sendTitle(p, "", ChatColor.YELLOW + ChatColor.BOLD.toString() + "Grace Period has ended!");
        });
    }

    public static void sendTitle(Player player, String title, String subtitle) {
        PacketPlayOutTitle playOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}"));
        PacketPlayOutTitle playOutSubtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}"));
        PacketPlayOutTitle playOutTimes = new PacketPlayOutTitle(20, 60, 20);

        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutTimes);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutTitle);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutSubtitle);
    }

    public static void sendAllTitle(String title, String sub){
        for(Player player : Bukkit.getOnlinePlayers()){
            PacketPlayOutTitle playOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}"));
            PacketPlayOutTitle playOutSubtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + sub + "\"}"));
            PacketPlayOutTitle playOutTimes = new PacketPlayOutTitle(20, 60, 20);

            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutTimes);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutTitle);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(playOutSubtitle);
        }
    }

    public static void announceBreakBlocks() {
        Bukkit.getOnlinePlayers().forEach(p -> Chat.sendTitle(p, ChatColor.AQUA + ChatColor.BOLD.toString() + "Deathmatch", ChatColor.YELLOW + "Block breaking has begun!"));
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "The blocks you placed in the center will start breaking! Beware!");
    }

    public static void announceWither() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "You are getting withered! Beware!");
    }

    public static void announceGlow() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "You are now glowing and visible to anyone! Beware!");
    }

    public static void announceHunger() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD+ "You are getting hungry faster! Beware!");
    }

    public static void announceDecrease1B1() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "The world border will shrink to 1 block over " + ((UWorldManager.DECREASE_TIME / 2) / 60) + " minutes!");
    }

    public static void announceAutoplaceBalance() {
        Bukkit.broadcastMessage(ChatColor.YELLOW + BOLD + "Auto assigning players to teams and balancing teams...");
    }

    public static void announceHost(){
        Bukkit.getOnlinePlayers().forEach(p -> Chat.sendTitle(p , ChatColor.GOLD + ChatColor.BOLD.toString() + "Ultra Hardcore" , ChatColor.YELLOW + "Hosted by " + ChatColor.GREEN + ChatColor.BOLD.toString() + "VanillaVerse" + ChatColor.YELLOW + "!"));
    }

    public static double roundToDecimilPlace(double d){
        DecimalFormat newFormat = new DecimalFormat("#.#");
        double x =  Double.valueOf(newFormat.format(d));
        return x;
    }

    public static void announceSettings(UhcGameSettings currentSettings) {
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "");
        Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "This game's configuration: ");

        Bukkit.broadcastMessage(ChatColor.YELLOW + "Player health: " + ChatColor.RED + (currentSettings.getHealth() / 2) + " ❤");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Border size: " + ChatColor.AQUA + currentSettings.getBorderSize() + " Blocks");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Team size: " + ChatColor.AQUA + currentSettings.getTeamSizes() + " Players");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Potions enabled: " + ChatColor.AQUA + (currentSettings.isPotionsEnabled() ? ChatColor.GREEN + "Yes" : ChatColor.RED + "No"));
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Regeneration on kill: " + ChatColor.AQUA + (currentSettings.isHealthGainOnKill() ? ChatColor.GREEN + "Yes" : ChatColor.RED + "No"));
        Bukkit.broadcastMessage(ChatColor.YELLOW + "Time: " + ChatColor.AQUA + (currentSettings.isEternalNight() ? "Eternal Night" : (currentSettings.isEternalDay() ? "Eternal Day" : "Starts at " + currentSettings.getInitialTime() +  " Ticks")));

    }

    public static void sendInActionbar(String message, Player player) {
        PacketPlayOutChat packet = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a((String)toDumbJSON(message)), (byte) 2);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)packet);
    }

    public static void sendAllActionbar(String message) {
        for(Player p : Bukkit.getOnlinePlayers()){
            PacketPlayOutChat packet = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a((String)toDumbJSON(message)), (byte) 2);
            ((CraftPlayer)p).getHandle().playerConnection.sendPacket((Packet)packet);
        }
    }

    public static String toDumbJSON(String input) {
        return "{\"text\":\"" + input + "\"}";
    }
}
