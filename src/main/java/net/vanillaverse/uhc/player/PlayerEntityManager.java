package net.vanillaverse.uhc.player;

import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.game.UhcGameSettings;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerEntityManager {

    public static void prepare(UPlayer uPlayer) {
        Player player = uPlayer.getPlayer();

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getActivePotionEffects().stream().forEach(p -> player.removePotionEffect(p.getType()));
        player.setMaxHealth(UhcGameSettings.getCurrentSettings().getHealth());
        player.setHealth(UhcGameSettings.getCurrentSettings().getHealth());
        player.setFoodLevel(100);
        player.setGameMode(uPlayer.isPlaying() ? GameMode.ADVENTURE : GameMode.SPECTATOR);
        player.setAllowFlight(!uPlayer.isPlaying());
        player.teleport(new Location(UWorldManager.getWorld(), 0, 201, 0));
    }

    public static void prepareForTP(UPlayer uPlayer) {
        Player player = uPlayer.getPlayer();

        player.getInventory().setArmorContents(null);
        player.getInventory().clear();
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10000000, 1, true));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10000000, 10, true));
        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 10000000, 200, true));
    }

    public static void prepareForPlay(UPlayer uPlayer) {
        Player player = uPlayer.getPlayer();

        player.setGameMode(GameMode.SURVIVAL);
        player.setFoodLevel(40);
        player.getActivePotionEffects().stream().forEach(p -> player.removePotionEffect(p.getType()));
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10 * 60, 4));
    }

    public static void prepareSpectator(UPlayer uPlayer) {
        Player player = uPlayer.getPlayer();
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getActivePotionEffects().stream().forEach(p -> player.removePotionEffect(p.getType()));
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(100);
        player.setGameMode(GameMode.SPECTATOR);
        player.setAllowFlight(true);
    }
}
