package net.vanillaverse.uhc.player;

import net.minecraft.server.v1_9_R1.DataWatcher;
import net.vanillaverse.uhc.team.UTeam;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by user on 23/07/2016.
 */
public class IdlePlayer {

    private Player name;
    private UTeam team;
    private Location logoutLocation;
    private long logoutTime;
    private ArrayList<ItemStack> inventoryContents;
    private ArrayList<ItemStack> armourContents;
    private double health;

    public IdlePlayer(Player name, UTeam team, Location location, ArrayList<ItemStack> stacks, ArrayList<ItemStack> armourContents, double hearts){
        this.inventoryContents = stacks;
        this.name = name;
        this.team = team;
        this.logoutLocation = location;
        this.logoutTime = System.currentTimeMillis();
        this.armourContents = armourContents;
        this.health = hearts;
    }

    public double getHealth(){
        return health;
    }

    public Location getLogoutLocation(){
        return logoutLocation;
    }

    public ArrayList<ItemStack> getArmourContents(){
        return armourContents;
    }

    public ArrayList<ItemStack> getInventory(){
        return inventoryContents;
    }

    public Player getName() {
        return name;
    }

    public UTeam getTeam() {
        return team;
    }

    public long getTime() {
        return logoutTime;
    }
}
