package net.vanillaverse.uhc.player;

import com.mysql.fabric.xmlrpc.base.Array;
import net.vanillaverse.database.Database;
import net.vanillaverse.database.Premium;
import net.vanillaverse.database.Rank;
import net.vanillaverse.database.utils.Menus;
import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.game.UhcGameSettings;
import net.vanillaverse.uhc.team.TeamHologram;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import net.vanillaverse.uhc.util.Connector;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

@SuppressWarnings("ALL")
public class PlayerListener implements Listener {

    public HashMap<String, IdlePlayer> rejoinPlayers = new HashMap<>();
    public long rejoinTime = 300000;

    public PlayerListener(){
        new BukkitRunnable(){
            @Override
            public void run() {
                for(String keys : rejoinPlayers.keySet()){
                    if(UGameManager.getStage() == 5){
                        if(System.currentTimeMillis() > rejoinPlayers.get(keys).getTime() + rejoinTime) {
                            Bukkit.getWorlds().get(0).strikeLightning(rejoinPlayers.get(keys).getLogoutLocation());
                            Bukkit.broadcastMessage(ChatColor.BOLD + rejoinPlayers.get(keys).getName().getName() + " didn't reconnect in time!");
                            for (ItemStack stacks : rejoinPlayers.get(keys).getInventory()) {
                                Bukkit.getWorlds().get(0).dropItemNaturally(rejoinPlayers.get(keys).getLogoutLocation(), stacks);
                            }
                            for (ItemStack stacks : rejoinPlayers.get(keys).getArmourContents()) {
                                Bukkit.getWorlds().get(0).dropItemNaturally(rejoinPlayers.get(keys).getLogoutLocation(), stacks);
                            }
                            rejoinPlayers.remove(keys);
                            UPlayer.removePlayer(rejoinPlayers.get(keys).getName());
                            UGameManager.checkForFinish();
                        }
                    }
                }
            }
        }.runTaskTimer(UHC.INSTANCE, 0, 20);
    }

    @EventHandler
    public void join(PlayerJoinEvent event) {
        event.setJoinMessage("");
        UPlayer uPlayer = UPlayer.getUPlayer(event.getPlayer());

        if (UGameManager.getStage() == 1 && UGameManager.getSlots() > UPlayer.getPlayingPlayers().size()) {
            uPlayer.setPlaying(true);
            uPlayer.setTeam(UTeamManager.getPlayersTeam());
        }

        if(rejoinPlayers.containsKey(uPlayer.getPlayer().getName())){
            event.getPlayer().setHealth(rejoinPlayers.get(uPlayer.getPlayer().getName()).getHealth());
            event.setJoinMessage(ChatColor.BOLD + event.getPlayer().getName() + " reconnected!");
            uPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
            uPlayer.setPlaying(true);
            uPlayer.setTeam(rejoinPlayers.get(event.getPlayer().getName()).getTeam());
            rejoinPlayers.remove(event.getPlayer().getName());
        }else{
            PlayerEntityManager.prepare(uPlayer);
        }
    }

    @EventHandler
    public void quit(PlayerQuitEvent event) {
        event.setQuitMessage("");
        UPlayer uPlayer = UPlayer.getUPlayer(event.getPlayer());

        if (uPlayer.isPlaying() && UGameManager.getStage() == 2) {
            TeamHologram.finish();
            UWorldManager.init(false);
            UGameManager.setStage(1);
            Chat.startCancel();
        }

        if(UGameManager.getStage() == 5 && uPlayer.isPlaying()){
            event.setQuitMessage(ChatColor.BOLD + event.getPlayer().getName() + " has disconnected! 5 minutes to rejoin.");
            ArrayList<ItemStack> armourStacks = new ArrayList<>();
            ArrayList<ItemStack> itemStacks = new ArrayList<>();
            for(ItemStack stack : event.getPlayer().getInventory().getArmorContents()){
                if(stack != null){
                    armourStacks.add(stack);
                }
            }
            for(ItemStack stack : event.getPlayer().getInventory()){
                if(stack != null){
                    itemStacks.add(stack);
                }
            }
            rejoinPlayers.put(event.getPlayer().getName(), new IdlePlayer(event.getPlayer(), uPlayer.getTeam(), event.getPlayer().getLocation(), itemStacks, armourStacks, event.getPlayer().getHealth()));
        }

        UPlayer.removePlayer(event.getPlayer());
    }

    @EventHandler
    public void damage(EntityDamageEvent event) {
        if (event.isCancelled()) return;
        event.setCancelled(UGameManager.getStage() != 5 || (event.getEntity() instanceof Player && UGameManager.isGrace()));
    }

    @EventHandler
    public void entityDamage(EntityDamageByEntityEvent event) {
        if (UGameManager.getStage() == 5 && UGameManager.isTeamed() && event.getEntity() instanceof Player && event.getDamager() instanceof Player) {

            Player damaged = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();

            if (UPlayer.getUPlayer(damaged).getTeam() == UPlayer.getUPlayer(damager).getTeam()) {
                event.setCancelled(true);
                return;
            }
        }
        if (UGameManager.getStage() == 5 && UGameManager.isTeamed() && event.getEntity() instanceof Player && event.getDamager() instanceof Projectile) {

            Player damaged = (Player) event.getEntity();
            Projectile damager = (Projectile) event.getDamager();

            if (damager.getShooter() instanceof Player) {
                if (UPlayer.getUPlayer(damaged).getTeam() == UPlayer.getUPlayer((Player) damager.getShooter()).getTeam()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBrew(BrewEvent event) {
        event.setCancelled(!UhcGameSettings.getCurrentSettings().isPotionsEnabled());
    }

    @EventHandler
    public void tp(PlayerTeleportEvent event) {
        if (event.getCause() == PlayerTeleportEvent.TeleportCause.NETHER_PORTAL) {
            event.setCancelled(!UhcGameSettings.getCurrentSettings().isNetherEnabled());
        }
    }

    @EventHandler
    public void rainbowRun(PlayerMoveEvent event){
        if(UGameManager.getStage() <= 4){
            if(UPlayer.getUPlayer(event.getPlayer()) != null || UPlayer.getUPlayer(event.getPlayer()).getTeam() != null){
                if(event.getTo().getX() != event.getFrom().getX() && event.getTo().getZ() != event.getFrom().getZ()){
                    if(UPlayer.getUPlayer(event.getPlayer()).getTeam() != UTeamManager.getSpectators()|| UPlayer.getUPlayer(event.getPlayer()).getTeam() != UTeamManager.getPlayersTeam()) {
                        if (event.getFrom().clone().add(0, -1, 0).getBlock().getType() == Material.STAINED_GLASS) {
                            int x =new Random().nextInt(10);
                            event.getFrom().clone().add(0 , -1 , 0).getBlock().setData(UPlayer.getUPlayer(event.getPlayer()).getTeam().getDyeColor().getData());
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void click(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof Player && event.getPlayer().getGameMode() == GameMode.SPECTATOR) {

            Player player = (Player) event.getRightClicked();

            Menus.Menu inventory = new Menus.Menu(player.getName() + "'s inventory", 5 * 9);

            for (int i = 0; i < 36; i++) {
                if (player.getInventory().getItem(i) != null) {
                    inventory.setItem(i, new Menus.MenuItem(player.getInventory().getItem(i), e -> {}));
                }
            }
            for (int i = 0; i < 4; i++) {
                if (player.getInventory().getArmorContents()[i] != null) {
                    inventory.setItem(i + 36, new Menus.MenuItem(player.getInventory().getArmorContents()[i], e -> {}));
                }
            }
            if (event.getPlayer().getInventory().getItemInOffHand() != null) {
                inventory.setItem(40, new Menus.MenuItem(event.getPlayer().getInventory().getItemInOffHand(), e -> {}));
            }

            inventory.showMenu(event.getPlayer());
        }
    }

    @EventHandler
    public void place(InventoryClickEvent event) {
        event.setCancelled(UGameManager.getStage() != 5);
    }

    @EventHandler
    public void place(BlockPlaceEvent event) {

        if(UGameManager.getStage() == 1){
            if(event.getBlock().getType() == Material.STAINED_CLAY){
                event.setCancelled(true);
                return;
            }

        }

        event.setCancelled(UGameManager.getStage() != 5);

        if(event.getBlock().getType() == Material.WORKBENCH || event.getBlock().getType() == Material.FURNACE){
            Location location = event.getBlock().getLocation();
            for(int x = (int)location.getX() + 1; x < location.getX() - 1; x++){
                for(int y = (int)location.getY() + 1; y < location.getY() - 1; y++){
                    for(int z = (int)location.getZ() + 1; z < location.getZ() - 1; z++){
                        Location aroundPoint = new Location(event.getBlock().getWorld(), x , y , z);
                        if(aroundPoint.getBlock().getType() == Material.WORKBENCH || aroundPoint.getBlock().getType() == Material.FURNACE){
                            event.getPlayer().sendMessage(ChatColor.RED + ChatColor.BOLD.toString() + "You cannot place this here during the deathmatch next to another!");
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void bbreak(BlockBreakEvent event) {
        event.setCancelled(UGameManager.getStage() != 5);
    }

    @EventHandler
    public void death(PlayerDeathEvent event) {

        if (event.getEntity().getKiller() != null) {
            UPlayer.getUPlayer(event.getEntity().getKiller()).addKill();
            event.setDeathMessage(ChatColor.RED + event.getDeathMessage().replace(event.getEntity().getName(), event.getEntity().getDisplayName() + ChatColor.RED)
                    .replace(event.getEntity().getKiller().getName(), event.getEntity().getKiller().getDisplayName() + ChatColor.RED)+ ".");
        } else {
            event.setDeathMessage(ChatColor.RED + event.getDeathMessage().replace(event.getEntity().getName(), event.getEntity().getDisplayName() + ChatColor.RED) + ".");
        }

        if (UGameManager.getStage() == 5) {
            if(UGameManager.isTeamed()){
                if(UTeamManager.getTeamSize(UPlayer.getUPlayer(event.getEntity()).getTeam()) == 1){
                    Chat.sendAllTitle(ChatColor.RED  +"✖", UPlayer.getUPlayer(event.getEntity()).getTeam().getChatColor() + ChatColor.BOLD.toString() + UPlayer.getUPlayer(event.getEntity()).getTeam().getName() + ChatColor.GREEN + ChatColor.BOLD.toString() + " has been eliminated!");
                }
            }else{
                Chat.sendAllTitle(ChatColor.RED +"✖", ChatColor.AQUA + ChatColor.BOLD.toString() + event.getEntity().getName() + ChatColor.AQUA + ChatColor.BOLD.toString() + " has been eliminated!");
            }

            event.getEntity().setMaxHealth(20.0);
            event.getEntity().setHealth(20.0);
            UPlayer uPlayer = UPlayer.getUPlayer(event.getEntity());
            uPlayer.setTeam(UTeamManager.getSpectators());
            uPlayer.setPlaying(false);
            event.getEntity().getWorld().strikeLightningEffect(event.getEntity().getLocation());

            PlayerEntityManager.prepareSpectator(uPlayer);
            UGameManager.checkForFinish();

            if (!Premium.hasPremium(event.getEntity()) && !Database.hasPermission(event.getEntity(), Rank.HELPER)) {
                Bukkit.getScheduler().runTaskLater(UHC.INSTANCE, () -> {
                    if (UGameManager.getStage() == 5) {
                        event.getEntity().sendMessage(
                                ChatColor.GREEN + "VV" + ChatColor.DARK_GREEN + ChatColor.BOLD.toString() + " > " +
                                        ChatColor.YELLOW + "You need "+ ChatColor.GOLD + "Premium" + ChatColor.YELLOW + " to spectate Ultra Hardcore");
                        event.getEntity().sendMessage(ChatColor.YELLOW + "Purchase at :" + ChatColor.GOLD + "store.vanillaverse.net");
                        Connector.connect(event.getEntity(), "Hub");
                    }
                }, 30*60L);
            }

            if (event.getEntity().getKiller() != null && UhcGameSettings.getCurrentSettings().isHealthGainOnKill()) {
                event.getEntity().getKiller().setHealth(event.getEntity().getKiller().getHealth() + 2);
            }

        } else {
            event.setDroppedExp(0);
            event.getDrops().clear();
        }
    }

    @EventHandler
    public void spectatorRob(PlayerInteractEvent event){
        if(event.getPlayer().getGameMode() == GameMode.SPECTATOR){
            if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
                Chat.sendInActionbar(ChatColor.RED + "You cannot do that while spectating!", event.getPlayer());
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void chat(AsyncPlayerChatEvent e) {
        UPlayer uPlayer = UPlayer.getUPlayer(e.getPlayer());
        if (UGameManager.getStage() != 6) {
            if (UGameManager.getStage() >= 4) {
                if (uPlayer.isPlaying()) {
                    if (UGameManager.isTeamed()) {
                        if (e.getMessage().startsWith("!")) {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(ChatColor.WHITE +  ChatColor.BOLD.toString() + "SHOUT " + uPlayer.getTeam().getPrefix()
                                        + e.getPlayer().getName() + ChatColor.WHITE + ChatColor.BOLD.toString() + " > " + ChatColor.WHITE + e.getMessage().substring(1));
                            });
                        } else {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                if (UPlayer.getUPlayer(p).getTeam() == uPlayer.getTeam()) {
                                    p.sendMessage(uPlayer.getTeam().getChatColor() + ChatColor.BOLD.toString() + "TEAM " + ChatColor.RESET
                                            +  e.getPlayer().getDisplayName() + ChatColor.WHITE + ChatColor.BOLD.toString() + " > " + ChatColor.WHITE + e.getMessage());
                                }
                            });
                        }
                    } else {
                        Bukkit.getOnlinePlayers().forEach(p -> {
                            p.sendMessage(uPlayer.getTeam().getChatColor() + ChatColor.BOLD.toString() + "SHOUT " + e.getPlayer().getDisplayName() + ChatColor.WHITE + ChatColor.BOLD.toString() + " > " + ChatColor.WHITE + e.getMessage());
                        });
                    }
                } else {
                    Bukkit.getOnlinePlayers().forEach(p -> {
                        if (!UPlayer.getUPlayer(p).isPlaying()) {
                            p.sendMessage(ChatColor.GRAY + ChatColor.BOLD.toString() + "SPEC " + ChatColor.RESET + e.getPlayer().getDisplayName() +
                                    ChatColor.WHITE + ChatColor.BOLD.toString() + " > " + ChatColor.WHITE + e.getMessage());
                        }
                    });
                }
                e.setCancelled(true);
            }
        }else{

        }
    }
}
