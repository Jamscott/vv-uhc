package net.vanillaverse.uhc.player;

import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.scoreboard.UBoard;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

public class UPlayer {

    private static HashMap<Player, UPlayer> cached = new HashMap<>();

    public static UPlayer getUPlayer(Player player) {

        if (!cached.containsKey(player)) {
            cached.put(player, new UPlayer(player));
        }

        return cached.get(player);
    }

    public static void removePlayer(Player player) {
        cached.remove(player);
    }

    public static HashMap<Player, UPlayer> getPlayers() {
        return cached;
    }

    private Player player;
    private boolean isPlaying = false;
    private UTeam team = UTeamManager.getSpectators();
    private int kills = 0;
    private UBoard scoreboard;

    public UPlayer(Player player) {
        this.player = player;
        this.scoreboard = new UBoard(Bukkit.getScoreboardManager().getNewScoreboard());
    }

    public static ArrayList<UPlayer> getPlayingPlayers() {
        return getPlayers().values().stream().filter(UPlayer::isPlaying).collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<UPlayer> getNotPlayingPlayers() {
        return getPlayers().values().stream().filter(u -> !u.isPlaying() || u.getTeam() == UTeamManager.getSpectators()).collect(Collectors.toCollection(ArrayList::new));
    }

    public void addKill(){
        kills++;
        if(UGameManager.isTeamed()){
            UTeamManager.addTeamKill(team);
        }
    }

    public UBoard getScoreboard(){
        return scoreboard;
    }

    public int getKills(){
        return kills;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public UTeam getTeam() {
        return team;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public void setTeam(UTeam team) {
        this.team = team;
    }
}