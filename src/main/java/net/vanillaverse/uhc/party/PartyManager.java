package net.vanillaverse.uhc.party;

import net.vanillaverse.uhc.UHC;
import org.bukkit.ChatColor;
import org.bukkit.Particle;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

/**
 * Created by user on 15/07/2016.
 */
public class PartyManager implements Listener {

    public static ArrayList<Party> parties = new ArrayList<>();

    public static ChatColor BODY = ChatColor.YELLOW;
    public static ChatColor OBJECT = ChatColor.GOLD;

    public PartyManager(){
        new BukkitRunnable(){
            public void run(){
                updateParties();
            }
        }.runTaskTimer(UHC.INSTANCE, 10, 20);
    }

    public static ArrayList<Party> getParties(){
        return parties;
    }

    public static void createParty(Player player){
        Party newParty = new Party();
        newParty.joinParty(player);
        parties.add(newParty);
    }

    public static Party getParty(Player player){
        for(Party parts : parties){
            if(parts.contains(player))
                return parts;
        }
        return null;
    }

    public void updateParties(){
        for(Party party : parties){
            if(party.getSize() == 0) {
                parties.remove(party);
                return;
            }
            party.expire();
        }
    }

    public static void clearParties(){
        parties.clear();
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event){
        if(getParty(event.getPlayer()) != null){
            getParty(event.getPlayer()).leaveParty(event.getPlayer());
        }
    }
}

