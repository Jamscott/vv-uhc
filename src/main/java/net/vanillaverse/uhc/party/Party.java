package net.vanillaverse.uhc.party;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 15/07/2016.
 */
public class Party {

    private Player leader;
    private ArrayList<Player> players = new ArrayList<>();
    private HashMap<Player, Long> invites = new HashMap<>();

    public Player getLeader(){
        return leader;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public int getNumOfInvites(){
        return invites.size();
    }

    public boolean contains(Player player){
        if(players.contains(player))
            return true;
        return false;
    }

    public boolean invitesContains(Player player){
        if(invites.containsKey(player))
            return true;
        return false;
    }

    public int getSize(){
        return this.players.size();
    }

    public void joinParty(Player player){
        if(players.isEmpty()){
            this.leader = player;
            this.players.add(player);
            partyMessage(player, "You created a new party!");
        }else{
            this.players.add(player);
            this.invites.remove(player);
            partyAnnounce(ChatColor.GOLD + player.getName() + PartyManager.BODY + " has joined the party!");
        }
    }

    public void kickPlayer(Player player){
        partyAnnounce(ChatColor.GOLD + player.getName() + PartyManager.BODY + " was kicked from the party by " + ChatColor.GOLD + leader.getName());
        this.players.remove(player);
    }

    public void inviteToParty(Player player){
        this.invites.put(player, System.currentTimeMillis());
        partyAnnounce(ChatColor.GOLD + player.getName() + PartyManager.BODY + " has been invite to the party!");
        partyMessage(player, "You have been invited to " + ChatColor.GOLD + leader.getName() + "'s " + PartyManager.BODY+ "party!");
        partyMessage(player, "To join type " + ChatColor.GOLD + "/party " + leader.getName());
    }

    public void leaveParty(Player player){
        partyAnnounce(ChatColor.GOLD + player.getName() + PartyManager.BODY + " has left the party!");
        this.players.remove(player);
        if(player == leader){
            if(this.players.size() >= 1){
                partyAnnounce("Party leadership has been transferred to " + ChatColor.GOLD + players.get(0).getName());
                leader = players.get(0);
            }
        }
    }

    public void expire(){
        for(Player player : this.invites.keySet()){
            if(System.currentTimeMillis() > this.invites.get(player) + (1000 * 30)){
                this.invites.remove(player);
                this.partyAnnounce(ChatColor.GOLD + player.getName() + "'s " + ChatColor.YELLOW + " party invite expired!");
            }
        }
    }

    public static void partyMessage(Player sender, String message){
        sender.sendMessage(ChatColor.GREEN + "Party " + ChatColor.DARK_GREEN + ChatColor.BOLD.toString() + "> " + PartyManager.BODY + message);
    }

    public void partyAnnounce(String message){
        for(Player player : players){
            partyMessage(player, message);
        }
    }


}
