package net.vanillaverse.uhc.game;

import com.google.gson.JsonObject;
import net.vanillaverse.database.Database;
import net.vanillaverse.database.Premium;
import net.vanillaverse.database.Rank;
import net.vanillaverse.database.ValueBank;
import net.vanillaverse.database.manager.ClientManager;
import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.party.Party;
import net.vanillaverse.uhc.party.PartyManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.PlayerEntityManager;
import net.vanillaverse.uhc.player.PlayerListener;
import net.vanillaverse.uhc.player.UPlayer;
import net.vanillaverse.uhc.team.TeamHologram;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import net.vanillaverse.uhc.tournament.RewardReveal;
import net.vanillaverse.uhc.util.Connector;
import net.vanillaverse.uhc.world.UTeleporter;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.craftbukkit.libs.org.ibex.nestedvm.util.Seekable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.ChatColor;
import org.bukkit.*;

import java.util.ArrayList;

@SuppressWarnings("ALL")
public class UGameManager {

    private static int stage = 0;
    private static int slots = 64;
    private static boolean isTeamed = true;
    private static boolean isTournament = false;
    private static boolean inDeathMatch = false;
    private static boolean forceTeams = false;
    private static int teamSizeLimit = 4;
    private static int teamNumber = UTeamManager.getTeams().size();
    private static int seconds = 30, sec = 0;
    private static int team = 0;
    private static UTeam twinner;
    private static UPlayer pwinner;
    private static boolean isPartiesEnabled = false;

    public static int getStage() {
        return stage;
    }

    public static boolean isTeamed() {
        return isTeamed;
    }

    public static int getSlots() {
        return slots;
    }

    public static boolean isGrace() {
        return graceTimer != 0;
    }

    public static boolean inDeathMatch() {
        return inDeathMatch;
    }

    public static boolean isIsParties(){
        return isPartiesEnabled;
    }

    /*
        STAGES:
        1: Players can join to play the game. Extra players will be automatically set to spectators.
        2: Player joins finished. There will be some seconds for the players to prepare for the next stage.
        3: If this is a tournament match, decide the winnings. Else, skip this step.
        4: Players get teleported to their spawnpoints in the world. Some time is given for chunkloads.
        5: The actual games starts. All the game logic will run here.
        6: The game finished. The earning are given and the players get collected to the winners statue.
        7: Players get kicked to the hub and the server reboots.
     */

    public static void progressStage() {
        switch (++stage) {
            case 1: {
                break;
            }
            case 2: {
                Chat.announceSettings(UhcGameSettings.getCurrentSettings());
                seconds = 16;
                UWorldManager.removeTeamPads();
                TeamHologram.finish();

                if (isTeamed()) {
                    Chat.announceAutoplaceBalance();
                    int peoplePerTeam = UPlayer.getPlayingPlayers().size() < teamNumber ? 1 : UPlayer.getPlayingPlayers().size() / (UPlayer.getPlayingPlayers().size() / teamSizeLimit);

                    if(isPartiesEnabled) {
                        partyAssign:{
                            for (Party party : PartyManager.getParties()) {
                                teamLoop:
                                {
                                    for (UTeam teams : UTeamManager.getTeams()) {
                                        if (UTeamManager.getTeamSize(teams) >= teamSizeLimit || (UTeamManager.getTeamSize(teams) + party.getSize()) > teamSizeLimit)
                                            continue;
                                        if ((UTeamManager.getTeamSize(teams) + party.getSize() <= teamSizeLimit)) {
                                            for (Player p : party.getPlayers()) {
                                                if (UTeamManager.isSoloOrSpec(UPlayer.getUPlayer(p).getTeam())) {
                                                    UTeamManager.switchTeam(UPlayer.getUPlayer(p), teams);
                                                    break teamLoop;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    playerTeam:{
                        for(UPlayer uPlayer : UPlayer.getPlayingPlayers()){
                            if(UTeamManager.isSoloOrSpec(uPlayer.getTeam())) {
                                assignTeam: {
                                    int counter = 0;
                                    for (UTeam team : UTeamManager.getTeams()) {
                                        if (UTeamManager.getTeamSize(team) < teamSizeLimit && UTeamManager.getTeamSize(team) <= peoplePerTeam) {
                                            UTeamManager.switchTeam(uPlayer, team);
                                            break assignTeam;
                                        } else {
                                            if(counter++ >= UGameManager.getTeamNumber()){
                                                break assignTeam;
                                            }
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                        if (uPlayer.getTeam() == UTeamManager.getPlayersTeam()) {
                            uPlayer.setPlaying(false);
                            uPlayer.setTeam(UTeamManager.getSpectators());

                            if (!Premium.hasPremium(uPlayer.getPlayer()) && !Database.hasPermission(uPlayer.getPlayer(), Rank.HELPER)) {
                                Bukkit.getScheduler().runTaskLater(UHC.INSTANCE, () -> {
                                    if (UGameManager.getStage() == 5) {
                                        uPlayer.getPlayer().sendMessage("§6Redirecting you to the Hub...");
                                        Connector.connect(uPlayer.getPlayer(), "Hub");
                                    }
                                }, 60*20L);
                            }
                        }
                    }
                }

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("type", "game_start");

                ClientManager.getInstance().sendCommand("uhc_action", jsonObject.toString());

                break;
            }
            case 3: {
                if (!isTournament) {
                    progressStage();
                    break;
                }

                RewardReveal.init();

                break;
            }
            case 4: {
                seconds = 0;
                team = 0;
                UWorldManager.removeTeamPads();

                break;
            }
            case 5: {
                seconds = -5;

                UWorldManager.removePlatform();
                UPlayer.getNotPlayingPlayers().forEach(PlayerEntityManager::prepareSpectator);

                break;
            }
            case 6: {
                if (isTeamed()) {
                    Chat.winner(twinner);
                } else {
                    Chat.winner(pwinner);
                }
                sec = 0;
                break;
            }
            case 7: {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("type", "game_stop");

                if (isTeamed) {
                    jsonObject.addProperty("teamWin", twinner.toString());
                } else {
                    jsonObject.addProperty("soloWin", pwinner.getPlayer().getUniqueId().toString());
                }

                ClientManager.getInstance().sendCommand("uhc_action", jsonObject.toString());
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.sendMessage("§6The game has been completed, redirecting you...");
                    Connector.connect(player, "Hub");
                }
                sec = 0;
            }
        }
    }

    public static void stopGame() {
        stage = 1;
        resetGame();

        Chat.startCancel();
    }

    private static int startSecs = -1;
    private static int graceTimer = 100;

    public static int getStartSec(){
        return startSecs;
    }

    public static void setStartSec(int secs){
        startSecs = secs;
    }

    public static void init() {

        progressStage();

        Bukkit.getScheduler().runTaskTimer(UHC.INSTANCE, () -> {
            if (stage == 1) {
                int count = 0;
                for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                    count++;
                }
                if (count >= slots / 4) {
                    if (startSecs == -1) {
                        startSecs = 120;
                    }
                } else {
                    if (startSecs != -1) {
                        startSecs = -1;
                    }
                }

                if (startSecs > 0) {
                    if (startSecs < 10 && startSecs > 0) {
                        Bukkit.getOnlinePlayers().forEach(p -> {
                            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 100, 10);
                            Chat.sendTitle(p, ChatColor.YELLOW + ChatColor.BOLD.toString() + startSecs, "");
                        });
                    }
                    startSecs--;
                    if (startSecs == 0) {
                        progressStage();
                    }
                }
            }
            if (stage == 2) {
                if (seconds-- == 1) {
                    Chat.announceHost();
                    Chat.announceTeamDis();
                    progressStage();
                    return;
                }
                if (seconds <= 10) {
                    Bukkit.getOnlinePlayers().forEach(p -> {
                        p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 100, 10);
                        Chat.sendTitle(p, ChatColor.YELLOW + ChatColor.BOLD.toString() + seconds, "");
                    });
                }
                if (seconds % 5 == 0 || seconds <= 5) {
                    Chat.gameDistr(seconds);
                }
            }
            if (stage == 4) {
                seconds++;
                if (seconds % 3 == 0) {
                    if (isTeamed) {
                        if (team == teamNumber) {
                            progressStage();
                            return;
                        }
                        UTeam uTeam = UTeamManager.getTeams().get(team++);
                        UTeleporter.teleportTeam(uTeam, angle);
                        angle += (2 * Math.PI / teamNumber);
                    } else {
                        UPlayer uPlayer = UPlayer.getPlayingPlayers().get(team++);
                        UTeleporter.teleportPlayer(uPlayer, angle);
                        angle += (2 * Math.PI / UPlayer.getPlayingPlayers().size());

                        if (team == UPlayer.getPlayingPlayers().size()) {
                            progressStage();
                            return;
                        }
                    }
                }
            }
            if (stage == 5) {
                if (++seconds == 0) {
                    Chat.announceGameStart();
                }
                if (seconds == 0) {
                    for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                        PlayerEntityManager.prepareForPlay(uPlayer);
                    }
                    UWorldManager.startGame();
                    graceTimer = 300;
                }
                if (graceTimer == 1) {
                    Chat.announceGraceEnd();
                }
                if (graceTimer != 0) {
                    if (seconds >= 0) {
                        String mins = (graceTimer / 60) + "";
                        if (mins.length() == 1) mins = "0" + mins;
                        String secs = (graceTimer % 60) + "";
                        if (secs.length() == 1) secs = "0" + secs;
                        Chat.sendAllActionbar(ChatColor.GREEN + "Grace period ends in " + ChatColor.BOLD + mins + ":" + secs);
                    }
                    graceTimer--;
                }
                if (seconds == UWorldManager.DECREASE_TIME) {
                    inDeathMatch = true;
                    Chat.announceBreakBlocks();
                    UBlockMemo.startBreak();
                }
                if (seconds >= UWorldManager.DECREASE_TIME + 600) {
                    if (seconds == UWorldManager.DECREASE_TIME + 600) {
                        for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                            if (uPlayer.getPlayer().getLocation().getY() < 40) {
                                uPlayer.getPlayer().sendMessage(ChatColor.YELLOW + "You are getting hungry! Get to the surface before it's too late!");
                                uPlayer.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 100000, 0));
                            }
                        }
                    }
                }
                if (seconds >= UWorldManager.DECREASE_TIME + 1200) {
                    if (seconds == UWorldManager.DECREASE_TIME + 1200) Chat.announceGlow();
                    for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                        uPlayer.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 100000, 0));
                    }
                }
                if (seconds == UWorldManager.DECREASE_TIME + 1800) {
                    Chat.announceDecrease1B1();
                    UWorldManager.border1B1();
                }
            }
            if (stage == 6) {
                if (sec++ == 15) {
                    progressStage();
                    return;
                }

                if (isTeamed()) {
                    for (UPlayer player : UPlayer.getPlayingPlayers()) {
                        if (player.getTeam() == twinner) {
                            playFirework(player.getPlayer());
                        }
                    }
                } else {
                    playFirework(pwinner.getPlayer());
                }
            }
            if (stage == 7) {
                if (sec++ == 5) {
                    Bukkit.shutdown();
                    progressStage();
                }
            }
        }, 20L, 20L);
    }

    private static void playFirework(Player player) {
        Firework firework = (Firework) player.getWorld().spawnEntity(player.getLocation().add(5 - Math.random() * 2.5, 0, 5 - Math.random() * 2.5), EntityType.FIREWORK);
        FireworkMeta fireworkMeta = firework.getFireworkMeta();
        if (isTeamed()) {
            fireworkMeta.addEffect(FireworkEffect.builder().withColor(twinner.getDyeColor().getColor()).withFlicker().withFade(dark(twinner.getDyeColor().getColor())).trail(true).build());
        } else {
            fireworkMeta.addEffect(FireworkEffect.builder().withColor(randomColor()).withFlicker().withFade(randomColor()).trail(true).build());
        }
        firework.setFireworkMeta(fireworkMeta);
    }

    private static Color dark(Color color) {
        return Color.fromRGB((int) (color.getRed() * 0.5), (int) (color.getGreen() * 0.5), (int)(color.getBlue() * 0.5));
    }

    private static Color randomColor() {
        int R = (int)(Math.random()*256);
        int G = (int)(Math.random()*256);
        int B= (int)(Math.random()*256);
        return Color.fromRGB(R, G, B);
    }

    private static double angle = 0D;

    public static int getTeamSizeLimit() {
        return teamSizeLimit;
    }

    public static void setSlots(int slots) {
        UGameManager.slots = slots;
        teamSizeLimit = slots / teamNumber;

        resetGame();
    }

    public static void setTeams(int teams) {
        if (teams == 0) {
            isTeamed = false;
        } else {
            isTeamed = true;
            teamNumber = teams;
            teamSizeLimit = slots / teamNumber;
        }
        resetGame();
    }

    public static void resetGame() {
        TeamHologram.finish();
        UWorldManager.init(false);

        PlayerListener playerListener = new PlayerListener();

        Bukkit.getOnlinePlayers().forEach(UPlayer::removePlayer);

        for (Player player : Bukkit.getOnlinePlayers()) {
            playerListener.join(new PlayerJoinEvent(player, ""));
        }
    }

    public static void setParty(boolean tf){
        isPartiesEnabled = tf;
    }

    public static int getTeamNumber() {
        return teamNumber;
    }

    public static int getSeconds() {
        return seconds;
    }

    public static void setTournament(boolean tournament) {
        UGameManager.isTournament = tournament;
    }

    public static void setStage(int stage) {
        UGameManager.stage = stage;
    }

    public static void checkForFinish() {
        if (isTeamed) {
            UTeam winner = null;
            for (UTeam uTeam : UTeamManager.getTeams()) {
                if (UTeamManager.isSoloOrSpec(uTeam)) continue;

                if (UTeamManager.getTeamPlayers(uTeam).size() != 0){
                    if (winner == null) winner = uTeam;
                    else return;
                }
            }

            if (winner == null) return;

            UGameManager.twinner = winner;

            for (UPlayer uPlayer : UPlayer.getPlayingPlayers()) {
                if (uPlayer.getTeam() == winner) {
                    ValueBank.statisticIncrement(uPlayer.getPlayer().getUniqueId(), 1, "uhc_teamedWin");
                    ValueBank.statisticIncrement(uPlayer.getPlayer().getUniqueId(), 1, "uhc_win");
                }
            }

            progressStage();

        } else {

            if (UPlayer.getPlayingPlayers().size() == 1) {
                UGameManager.pwinner = UPlayer.getPlayingPlayers().get(0);
                ValueBank.statisticIncrement(pwinner.getPlayer().getUniqueId(), 1, "uhc_soloWin");
                ValueBank.statisticIncrement(pwinner.getPlayer().getUniqueId(), 1, "uhc_win");
                progressStage();
            }

        }
    }
}
