package net.vanillaverse.uhc.game;

public class UhcGameSettings {

    public static UhcGameSettings currentSettings;
    private int health = 20, borderSize = 2000, teamSizes = 4, initialTime = 0;
    private boolean netherEnabled = true,
            potionsEnabled = true,
            eternalNight = false,
            eternalDay = false,
            healthGainOnKill = false;

    public static UhcGameSettings getCurrentSettings() {
        return currentSettings;
    }

    public int getHealth() {
        return health;
    }

    public int getBorderSize() {
        return borderSize;
    }

    public int getTeamSizes() {
        return teamSizes;
    }

    public int getInitialTime() {
        return initialTime;
    }

    public boolean isNetherEnabled() {
        return netherEnabled;
    }

    public boolean isPotionsEnabled() {
        return potionsEnabled;
    }

    public boolean isEternalNight() {
        return eternalNight;
    }

    public boolean isEternalDay() {
        return eternalDay;
    }

    public boolean isHealthGainOnKill() {
        return healthGainOnKill;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setBorderSize(int borderSize) {
        this.borderSize = borderSize;
    }

    public void setTeamSizes(int teamSizes) {
        this.teamSizes = teamSizes;
    }

    public void setInitialTime(int initialTime) {
        this.initialTime = initialTime;
    }

    public void setNetherEnabled(boolean netherEnabled) {
        this.netherEnabled = netherEnabled;
    }

    public void setPotionsEnabled(boolean potionsEnabled) {
        this.potionsEnabled = potionsEnabled;
    }

    public void setEternalNight(boolean eternalNight) {
        this.eternalNight = eternalNight;
    }

    public void setEternalDay(boolean eternalDay) {
        this.eternalDay = eternalDay;
    }

    public void setHealthGainOnKill(boolean healthGainOnKill) {
        this.healthGainOnKill = healthGainOnKill;
    }
}
