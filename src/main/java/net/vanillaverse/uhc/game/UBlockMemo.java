package net.vanillaverse.uhc.game;

import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class UBlockMemo implements Listener {


    //TEST
    public static HashMap<Player, ArrayList<Block>> upb = new HashMap<>();
    public static int task;

    @EventHandler
    public void onplace(BlockPlaceEvent event) {
        if (UGameManager.getStage() != 5) return;
        if (event.getBlock().getType() == Material.TORCH || event.getBlock().getType() == Material.ENCHANTMENT_TABLE || event.getBlock().getType() == Material.WORKBENCH || event.getBlock().getType() == Material.ANVIL || event.getBlock().getType() == Material.FURNACE|| event.getBlock().getType() == Material.OBSIDIAN) return;
        if (Math.abs(event.getBlock().getX()) <= UWorldManager.FINAL_SIZE / 2 && Math.abs(event.getBlock().getZ()) <= UWorldManager.FINAL_SIZE / 2) {
            if (!upb.containsKey(event.getPlayer())) upb.put(event.getPlayer(), new ArrayList<>());
            upb.get(event.getPlayer()).add(event.getBlock());
        }
    }

    public static void startBreak() {
        task = Bukkit.getScheduler().runTaskTimer(UHC.INSTANCE, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!upb.containsKey(player) || upb.get(player).size() < 3) continue;

                Block b = upb.get(player).remove(0);
                b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, b.getType());
                b.breakNaturally(new ItemStack(Material.DIAMOND_PICKAXE));

                if (upb.get(player).size() == 0) upb.remove(player);
            }

            if (upb.size() == 0) {
                Bukkit.getScheduler().cancelTask(task);
            }

        }, 10L, 10L).getTaskId();


    }

}
