package net.vanillaverse.uhc.command;

import net.vanillaverse.database.Chat;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.party.Party;
import net.vanillaverse.uhc.party.PartyManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by user on 15/07/2016.
 */
public class PartyCommand implements CommandExecutor {

    @SuppressWarnings("ALL")
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        Player player = (Player) commandSender;
        Party party = PartyManager.getParty(player);

        if(UGameManager.getStage() < 3) {
            if(!UGameManager.isIsParties()) {
                Party.partyMessage(player, ChatColor.RED + "Parties are currently disabled!");
                return false;
            }
            if(args.length == 1) {
                if (args[0].equalsIgnoreCase("leave")) {
                    if (PartyManager.getParty(player) != null)
                        PartyManager.getParty(player).leaveParty(player);
                    else
                        Party.partyMessage(player, ChatColor.RED + "You're not currently in a party!");
                    return true;
                }
                Player target = Bukkit.getPlayer(args[0]);

                if(target != null){
                    if(target == player){
                        Party.partyMessage(player, "You can't party with yourself!");
                    }else {
                        if(party != null){
                            if(party.getSize() + party.getNumOfInvites() >= UGameManager.getTeamSizeLimit()){
                                party.partyMessage(player, "Your party can't be larger than " + UGameManager.getTeamSizeLimit() + " players!");
                            }else if(party.getPlayers().contains(target)){
                                party.partyMessage(player, target.getName() + " is already in the party!");
                            }else if(party.invitesContains(target)){
                                party.partyMessage(player, target.getName() + " is already been invited to your party!");
                            }else if(party.getLeader() == player){
                                party.inviteToParty(target);
                            }else{
                                party.partyAnnounce(player.getName() + " suggested " + ChatColor.GOLD + target.getName() + ChatColor.YELLOW + " to be invited to the party!");
                                party.partyMessage(party.getLeader(), "To invite " + ChatColor.GOLD + target.getName() + ChatColor.YELLOW + " type /party " + target.getName());
                            }
                        }else{
                            Party p2 = PartyManager.getParty(target);
                            if(p2 != null && p2.invitesContains(player)){
                                p2.joinParty(player);
                                return true;
                            }
                            PartyManager.createParty(player);
                        }
                    }
                }else{
                    Party.partyMessage(player, "That player is not online!");
                }
            }else if(args.length == 2){
                if(args[0].equalsIgnoreCase("kick")){
                    if(PartyManager.getParty(player) != null){
                        Player player1 = Bukkit.getPlayer(args[1]);
                        if(player1 != null){
                            if(PartyManager.getParty(player1) == PartyManager.getParty(player)){
                                PartyManager.getParty(player).kickPlayer(player1);
                                return true;
                            }else{
                                Party.partyMessage(player, ChatColor.GOLD  + "That player is not in your party!");
                                return true;
                            }
                        }else{
                            Party.partyMessage(player, ChatColor.GOLD  + "That player is currently offline!");
                            return true;
                        }
                    }else{
                        Party.partyMessage(player, ChatColor.GOLD  + "Your currently not in a party!");
                        return true;
                    }
                }
            }else{
                Party.partyMessage(player, ChatColor.YELLOW + ChatColor.BOLD.toString() + "UHC Party commands:");
                Party.partyMessage(player, ChatColor.GOLD  + "/party <name> - Join/Create party");
                Party.partyMessage(player, ChatColor.GOLD  + "/party leave - Leaves current party");
                Party.partyMessage(player, ChatColor.GOLD  + "/party kick <name> - Kicks player from party");
                return true;
            }
        }else{
            Party.partyMessage(player, ChatColor.RED + "Parties are disabled while the game is live!");
            return true;
        }
        return false;
    }
}


