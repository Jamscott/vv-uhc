package net.vanillaverse.uhc.command;

import net.vanillaverse.database.VanillaPlayer;
import net.vanillaverse.uhc.game.UBlockMemo;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.party.PartyManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.UPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SettingsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if (args.length == 0) {

        } else if (args[0].equalsIgnoreCase("setSlots")) {

            int target = Integer.parseInt(args[1]);
            if (!UGameManager.isTeamed() && target < 2) {
                commandSender.sendMessage(ChatColor.RED + "There need to be at least 2 slots open!");
                return true;
            } else if (UGameManager.isTeamed() && target % UGameManager.getTeamNumber() != 0) {
                commandSender.sendMessage(ChatColor.RED + "Slots must a multiple of " + UGameManager.getTeamNumber() + "!");
                return true;
            }

            UGameManager.setSlots(target);
            Chat.sendChangeSlots(commandSender, Integer.parseInt(args[1]));

        } else if (args[0].equalsIgnoreCase("setTournament")) {

            boolean target = Boolean.parseBoolean(args[1]);
            UGameManager.setTournament(target);
            Chat.sendChangeTournament(commandSender, target);

        } else if (args[0].equalsIgnoreCase("setTeams")) {

            int target = Integer.parseInt(args[1]);

            if (target != 0 && target < 2) {
                commandSender.sendMessage(ChatColor.RED + "There need to be 0 or at least 2 teams!");
                return true;
            }

            UGameManager.setTeams(Integer.parseInt(args[1]));
            Chat.sendSetTeams(commandSender, Integer.parseInt(args[1]));
        } else if (args[0].equalsIgnoreCase("start")) {

            if (UPlayer.getPlayingPlayers().size() < 2) {
                Chat.sendNotEnoughPlayers(commandSender);
                return true;
            }

            if (UGameManager.getStage() == 1) {
                UGameManager.progressStage();
            } else{
                Chat.sendAlreadyStarted(commandSender);
            }

        } else if (args[0].equalsIgnoreCase("forcetp")) {

            UGameManager.setStage(1);
            UGameManager.setTournament(false);
            UGameManager.progressStage();
            UGameManager.progressStage();

        } else if (args[0].equalsIgnoreCase("forceb")) {

            UBlockMemo.startBreak();

        } else if (args[0].equalsIgnoreCase("delay")) {

            UGameManager.setStartSec(UGameManager.getStartSec() + 30);
            Chat.sendAllActionbar(ChatColor.YELLOW + Chat.BOLD + commandSender.getName() + " delayed the countdown!");

        } else if (args[0].equalsIgnoreCase("toggleParties")) {

            UGameManager.setStartSec(UGameManager.getStartSec() + 30);
            UGameManager.setParty(!UGameManager.isIsParties());
            Chat.sendAllActionbar(ChatColor.YELLOW + Chat.BOLD + commandSender.getName() + (UGameManager.isIsParties() ? " enabled" : " disabled") + " parties!");
            PartyManager.clearParties();
            UGameManager.setTeams(UGameManager.getTeamNumber());

        }

        return true;
    }
}
