package net.vanillaverse.uhc.command;

import net.minecraft.server.v1_9_R1.MinecraftServer;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.UPlayer;
import net.vanillaverse.uhc.team.TeamHologram;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.entity.Player;

public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && (UGameManager.getStage() == 1 || UGameManager.getStage() == 2) && UGameManager.isTeamed()) {
            Chat.sendLeftTeam(commandSender);
            UPlayer uPlayer = UPlayer.getUPlayer((Player) commandSender);
            if (uPlayer.isPlaying() && UGameManager.getStage() == 2) {
                UWorldManager.init(false);
                UGameManager.setStage(1);
                Chat.startCancel();
            }

            UTeamManager.switchTeam(uPlayer, UTeamManager.getSpectators());
        }
        double a = MinecraftServer.getServer().recentTps[0];
        return false;
    }
}
