package net.vanillaverse.uhc.world;

import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.player.PlayerEntityManager;
import net.vanillaverse.uhc.player.UPlayer;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

@SuppressWarnings("Duplicates")
public class UTeleporter {

    public static String teamName;

    public static void teleportTeam(UTeam uTeam, double angle) {

        int x, y, z;
        Block block;
        do {
            x = (int) ((UWorldManager.INITIAL_SIZE / 2.5) * Math.cos(angle));
            z = (int) ((UWorldManager.INITIAL_SIZE / 2.5) * Math.sin(angle));
            y = UWorldManager.getWorld().getHighestBlockYAt(x, z);
            block = UWorldManager.getWorld().getBlockAt(x, y - 1, z);
            angle += Math.PI / (UGameManager.getTeamNumber() * 16);
        } while (!block.getType().isSolid());

        for (UPlayer uPlayer : UTeamManager.getTeamPlayers(uTeam)) {
            uPlayer.getPlayer().teleport(new Location(UWorldManager.getWorld(), x, y + 3, z));
            uPlayer.getPlayer().getInventory().setChestplate(null);
            PlayerEntityManager.prepareForTP(uPlayer);
        }

        Bukkit.getScheduler().runTaskLater(UHC.INSTANCE, () -> {
            for (UPlayer uPlayer : UTeamManager.getTeamPlayers(uTeam)) {
                uPlayer.getPlayer().teleport(uPlayer.getPlayer().getLocation().add(0, 1, 0));
            }
        }, 20L);

        teamName = uTeam.toString();
        Bukkit.getLogger().info("Teleporting the " + uTeam.getName() + " team...");

    }

    public static void teleportPlayer(UPlayer uPlayer, double angle) {
        int x, y, z;
        Block block;
        do {
            x = (int) ((UWorldManager.INITIAL_SIZE / 2.5) * Math.cos(angle));
            z = (int) ((UWorldManager.INITIAL_SIZE / 2.5) * Math.sin(angle));
            y = UWorldManager.getWorld().getHighestBlockYAt(x, z);
            block = UWorldManager.getWorld().getBlockAt(x, y - 1, z);
            angle += Math.PI / (UGameManager.getTeamNumber() * 8);
        } while (!block.getType().isSolid());

        uPlayer.getPlayer().teleport(new Location(UWorldManager.getWorld(), x, y + 3, z));
        PlayerEntityManager.prepareForTP(uPlayer);

        Bukkit.getScheduler().runTaskLater(UHC.INSTANCE, () -> {
            uPlayer.getPlayer().teleport(uPlayer.getPlayer().getLocation().add(0, 1, 0));
        }, 20L);

        Bukkit.getLogger().info("Teleporting " + uPlayer.getPlayer());
    }
}
