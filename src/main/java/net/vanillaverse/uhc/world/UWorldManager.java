package net.vanillaverse.uhc.world;

import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.game.UhcGameSettings;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.team.TeamHologram;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.scoreboard.Team;

public class UWorldManager {


    public final static int DECREASE_TIME = 3600;
    public static int INITIAL_SIZE = 2000;
    public final static int FINAL_SIZE = 100;

    public static World world;
    public static World nether;

    public static void init(boolean firstTime) {

        if (firstTime) {
            world = Bukkit.getWorlds().get(0);
            world.setSpawnLocation(0, world.getHighestBlockYAt(0, 0), 0);
            world.getWorldBorder().setCenter(0.5, 0.5);
            world.getWorldBorder().setSize(INITIAL_SIZE + .5);
            world.setTime(6000);
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setGameRuleValue("naturalRegeneration", "false");


            nether = Bukkit.getWorlds().get(1);
            nether.getWorldBorder().setCenter(0.5, 0.5);
            nether.getWorldBorder().setSize(INITIAL_SIZE + .5);
            nether.setGameRuleValue("naturalRegeneration", "false");

            nether.getEntities().stream().filter(entity -> entity.getLocation().getY() > 180).forEach(Entity::remove);
        }

        UWorldManager.removeTeamPads();
        UTeamManager.generateTeams();

        Location center = new Location(world, 0, 199, 0);
        for (int x = -30; x <= 30; x++) {
            for (int z = -30; z <= 30; z++) {
                Location location = center.clone().add(x, 0, z);
                if (center.distance(location) < 29) {
                    Block block = location.getBlock();
                    if (block.getType() == Material.STAINED_CLAY && !firstTime) {
                        block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, Material.STAINED_CLAY,(int) block.getData());
                    }
                    block.setType(Material.STAINED_GLASS);
                    block.setData((byte) 7);
                    block.getRelative(BlockFace.UP).setType(Material.AIR);
                } else if (center.distance(location) > 29) {
                    location.clone().add(0, 1, 0).getBlock().setType(Material.BARRIER);
                    location.clone().add(0, 2, 0).getBlock().setType(Material.BARRIER);
                }
            }
        }

        if (UGameManager.isTeamed() && !UGameManager.isIsParties()) {
            int count = UGameManager.getTeamNumber();

            double diff = 2 * Math.PI / count;
            double angle = 0;

            int ccount = 0;

            int rad = UTeamManager.getTeams().size() > 20 ? 25 : 15;

            for (UTeam team : UTeamManager.getTeams()) {
                if (UTeamManager.isSoloOrSpec(team)) continue;

                int x = (int) Math.round(rad * Math.cos(angle));
                int z = (int) Math.round(rad * Math.sin(angle));

                for (int xx = -1; xx <= 1; xx++) {
                    for (int zz = -1; zz <= 1; zz++) {
                        Block block = center.clone().add(xx + x, 0, zz + z).getBlock();

                        block.setType(Material.STAINED_CLAY);
                        block.setData(team.getDyeColor().getWoolData());
                    }
                }

                TeamHologram.makeArmorStand(center.clone().add(x + 0.5, 1, z  + 0.5), team);

                angle += diff;

                if (++ccount >= UGameManager.getTeamNumber()) break;
            }
            TeamHologram.init();
        }
    }

    public static void removeTeamPads() {
        Location center = new Location(world, 0, 199, 0);
        for (int x = -30; x <= 30; x++) {
            for (int z = -30; z <= 30; z++) {
                Location location = center.clone().add(x, 0, z);
                if (center.distance(location) < 29) {
                    Block block = location.getBlock();
                    if (block.getType() == Material.STAINED_CLAY) {
                        block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, Material.STAINED_CLAY,(int) block.getData());
                    }
                    block.setType(Material.STAINED_GLASS);
                    block.setData((byte) 7);
                } else if (center.distance(location) == 29) {
                    location.clone().add(0, 1, 0).getBlock().setType(Material.BARRIER);
                    location.clone().add(0, 2, 0).getBlock().setType(Material.BARRIER);
                }
            }
        }
    }

    public static void startGame() {
        Bukkit.getScheduler().runTaskLater(UHC.INSTANCE, () -> {
            world.getWorldBorder().setSize(FINAL_SIZE + .5, DECREASE_TIME);
            nether.getWorldBorder().setSize(FINAL_SIZE + .5, DECREASE_TIME);
            Chat.announceDecrease();
        }, 100L);

        UhcGameSettings gs = UhcGameSettings.getCurrentSettings();

        if (gs.isEternalDay()) {
            world.setTime(6000);
            world.setGameRuleValue("doDaylightCycle", "false");
        } else if (gs.isEternalNight()) {
            world.setTime(18000);
            world.setGameRuleValue("doDaylightCycle", "false");
        } else {
            world.setTime(gs.getInitialTime());
            world.setGameRuleValue("doDaylightCycle", "true");
        }
    }

    public static void border1B1() {
        world.getWorldBorder().setSize(0.5, DECREASE_TIME / 2);
    }

    public static World getWorld() {
        return world;
    }

    public static void removePlatform() {
        Location center = new Location(world, 0, 199, 0);
        for (int x = -40; x <= 40; x++) {
            for (int z = -40; z <= 40; z++) {
                Location location = center.clone().add(x, 0, z);
                if (center.distance(location) < 31) {
                    Block block = location.getBlock();
                    block.setType(Material.AIR);
                    block.getRelative(BlockFace.UP).setType(Material.AIR);
                } else if (center.distance(location) < 40) {
                    location.clone().add(0, 1, 0).getBlock().setType(Material.AIR);
                    location.clone().add(0, 2, 0).getBlock().setType(Material.AIR);
                    location.clone().add(0, 3, 0).getBlock().setType(Material.AIR);
                }
            }
        }
    }

    public void outsideBorder(Location location){

    }
}
