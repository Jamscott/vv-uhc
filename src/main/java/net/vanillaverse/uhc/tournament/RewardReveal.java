package net.vanillaverse.uhc.tournament;

import net.minecraft.server.v1_9_R1.BlockPosition;
import net.minecraft.server.v1_9_R1.Blocks;
import net.minecraft.server.v1_9_R1.MinecraftServer;
import net.minecraft.server.v1_9_R1.PacketPlayOutBlockAction;
import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.world.UWorldManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Random;

public class RewardReveal implements Listener {

    public static int ticks = 0;
    public static Block chest1, chest2, chest3;
    public static ArmorStand reward1, reward2, reward3;
    public static Item item1, item2, item3;
    public static int rewardRev = 0, task;

    public static int chunks;
    public static int crates;

    public static void init() {

        Random random = new Random();
        chunks = (5 + random.nextInt(16)) * 1000;
        crates = 3 + random.nextInt(11);


        World world = UWorldManager.getWorld();
        for (int x = -4; x <= 4; x++) {
            for (int z = 7; z <= 9; z++) {
                Location location = new Location(world, x, 199, z);
                location.getBlock().setType(Material.STAINED_CLAY);
                location.getBlock().setData((byte) (z == 8 && (x < 4 && x > -4) ? 0 : 8));
            }
        }

        chest1 = world.getBlockAt(3, 200, 8);
        chest2 = world.getBlockAt(0, 200, 8);
        chest3 = world.getBlockAt(-3, 200, 8);

        chest1.setType(Material.CHEST);
        chest2.setType(Material.CHEST);
        chest3.setType(Material.CHEST);

        ticks = 0;

        task = Bukkit.getScheduler().runTaskTimer(UHC.INSTANCE, () -> {

            if (ticks == 1) {
                Chat.announceRewards();
            } else if (ticks == 3) {
                open(chest1, 1);
                rewardRev = 1;
            } else if (ticks == 6) {
                open(chest2, 2);
                rewardRev = 2;
            } else if (ticks == 9) {
                open(chest3, 3);
                rewardRev = 3;
            }

            if (ticks == 15) {
                chest1.getWorld().playEffect(chest1.getLocation(), Effect.STEP_SOUND, Material.CHEST);
                chest1.getWorld().playEffect(chest2.getLocation(), Effect.STEP_SOUND, Material.CHEST);
                chest1.getWorld().playEffect(chest3.getLocation(), Effect.STEP_SOUND, Material.CHEST);
                chest1.setType(Material.AIR);
                chest2.setType(Material.AIR);
                chest3.setType(Material.AIR);
                for (int x = -5; x <= 5; x++) {
                    for (int z = 7; z <= 9; z++) {
                        Location location = new Location(world, x, 199, z);
                        location.getBlock().setType(Material.STAINED_GLASS);
                        location.getBlock().setData((byte) 7);
                    }
                }
                item1.remove();
                item2.remove();
                item3.remove();
                reward1.remove();
                reward2.remove();
                reward3.remove();

                Bukkit.getScheduler().cancelTask(task);
                UGameManager.progressStage();
            }

            ticks++;
        }, 20L, 20L).getTaskId();
    }

    public static void open(Block block, int step) {
        for (Player player1 : Bukkit.getOnlinePlayers()) {
            CraftPlayer p = (CraftPlayer) player1;
            PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(new BlockPosition(block.getX(), block.getY(), block.getZ()), Blocks.CHEST, 1, 1);
            p.getHandle().playerConnection.sendPacket(packet);
        }

        ArmorStand armorStand = (ArmorStand) block.getWorld().spawnEntity(block.getLocation().add(0.5, -1, 0.5), EntityType.ARMOR_STAND);
        armorStand.setGravity(false);
        armorStand.setVisible(false);

        ItemStack itemStack =  new ItemStack(step == 1 ? Material.GOLD_INGOT : step == 2 ? Material.POTION : Material.NAME_TAG);
        ItemMeta itemMeta = itemStack.getItemMeta();

        //itemMeta.setDisplayName(step == 1 ? ChatColor.GREEN + "" + chunks + " Chunks" : step == 2 ? ChatColor.GREEN + "2x " + " Blast" : ChatColor.LIGHT_PURPLE + "#Victor" + ChatColor.RED + Chat.BOLD +" title");
        itemStack.setItemMeta(itemMeta);

        Item item = block.getWorld().dropItem(block.getLocation(), itemStack);
        item.setPickupDelay((int) 1E10);
        item.setCustomNameVisible(true);
        item.setCustomName(step == 1 ? ChatColor.GREEN + "" + chunks + " Chunks" : step == 2 ? ChatColor.GREEN + "" + crates + " Tier V Crates" : ChatColor.LIGHT_PURPLE +  "#Victor" + ChatColor.GREEN + " Title");
        armorStand.setPassenger(item);

        String reward = step == 1 ? ChatColor.GREEN + Chat.BOLD + "" + chunks + " Chunks" : step == 2 ? ChatColor.GREEN +  Chat.BOLD + crates + " Tier V Crates" : ChatColor.LIGHT_PURPLE +  Chat.BOLD + "#Victor" + ChatColor.GREEN +  Chat.BOLD + " Title";

        Chat.announceReward(reward, step);

        if (step == 1) {
            reward1 = armorStand;
            item1 = item;
        } else if (step == 2) {
            reward2 = armorStand;
            item2 = item;
        } else if (step == 3) {
            reward3 = armorStand;
            item3 = item;
        }
    }

    public static String getRewardString(int step) {
        return step == 1 ? ChatColor.GREEN + Chat.BOLD + "" + chunks + " Chunks" : step == 2 ? ChatColor.GREEN +  Chat.BOLD + crates + " Tier V Crates" : ChatColor.LIGHT_PURPLE +  Chat.BOLD + "#Victor" + ChatColor.GREEN +  Chat.BOLD + " Title";
    }

    @EventHandler
    public void onOpen(PlayerInteractEvent event) {
        if (UGameManager.getStage() != 3) return;
        if (event.getClickedBlock().getType() == Material.CHEST && event.getClickedBlock().getY() == 200) {
            if (event.getClickedBlock().getX() == chest1.getX() || event.getClickedBlock().getX() == chest2.getX() || event.getClickedBlock().getX() == chest3.getX()) {
                event.setCancelled(true);
            }
        }
    }

}
