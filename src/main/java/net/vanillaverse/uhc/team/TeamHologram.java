package net.vanillaverse.uhc.team;

import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.player.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.HashMap;

public class TeamHologram {

    public static HashMap<UTeam, ArmorStand> armorStandHashMap = new HashMap<>();
    public static Integer task;

    public static ArmorStand getTeamStand(UTeam team){
        return armorStandHashMap.get(team);
    }

    public static void makeArmorStand(Location location, UTeam team) {
        ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setCustomNameVisible(true);
        armorStandHashMap.put(team, armorStand);
        updateArmorStand(team);
    }

    public static void init() {
        task = Bukkit.getScheduler().runTaskTimer(UHC.INSTANCE, () -> {
            armorStandHashMap.keySet().forEach(TeamHologram::updateArmorStand);
        }, 3L, 3L).getTaskId();
    }

    public static void finish() {
        Bukkit.getScheduler().cancelTask(task);
        armorStandHashMap.values().forEach(ArmorStand::remove);
        armorStandHashMap.clear();
    }

    private static void updateArmorStand(UTeam uTeam) {
        if (!armorStandHashMap.containsKey(uTeam)) return;
        ArmorStand armorStand = armorStandHashMap.get(uTeam);

        int count = UTeamManager.getTeamSize(uTeam);

        armorStand.setCustomName(uTeam.getChatColor() + Chat.BOLD + "Team " + uTeam.getName() + ChatColor.RESET + (count == UGameManager.getTeamSizeLimit() ? ChatColor.RED :  ChatColor.YELLOW)+ " [" + count + "/" + UGameManager.getTeamSizeLimit() + "]");
    }

}
