package net.vanillaverse.uhc.team;

import com.mysql.fabric.xmlrpc.base.Array;
import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.player.UPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;


public class UTeam{

    private String name;
    private ChatColor chatColor;

    public UTeam(String name, ChatColor chatColor){
        this.name = name;
        this.chatColor = chatColor;
    }

    public void setChatColor(ChatColor chatColor){
        this.chatColor = chatColor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public DyeColor getDyeColor() {
        if(chatColor == ChatColor.YELLOW) return DyeColor.YELLOW;
        else if(chatColor == ChatColor.BLACK) return DyeColor.BLACK;
        else if(chatColor == ChatColor.GREEN) return DyeColor.GREEN;
        else if(chatColor == ChatColor.DARK_GREEN) return DyeColor.GREEN;
        else if(chatColor == ChatColor.RED) return DyeColor.RED;
        else if(chatColor == ChatColor.DARK_RED) return DyeColor.RED;
        else if(chatColor == ChatColor.AQUA) return DyeColor.LIGHT_BLUE;
        else if(chatColor == ChatColor.DARK_AQUA) return DyeColor.BLUE;
        else if(chatColor == ChatColor.GOLD) return DyeColor.ORANGE;
        else if(chatColor == ChatColor.DARK_BLUE) return DyeColor.BLUE;
        else if(chatColor == ChatColor.BLUE) return DyeColor.LIGHT_BLUE;
        return DyeColor.WHITE;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public String getPrefix(){
        return chatColor + ChatColor.BOLD.toString() + name + " ";
    }

    public static enum TeamNames{

        DARK_BLUE(ChatColor.BLUE, DyeColor.BLUE, new String[]{"Water" , "Fish" , "Shark", "Ocean" , "Guardian"}),
        DARK_GREEN(ChatColor.DARK_GREEN, DyeColor.GREEN, new String[]{"Slime" , "Zombie" , "Hulk", "Leaf"}),
        DARK_RED(ChatColor.DARK_RED, DyeColor.RED, new String[]{"Blood" , "Hell" , "Nether", "Redepic" , "Deadpool"}),
        DARK_PURPLE(ChatColor.DARK_PURPLE, DyeColor.PURPLE, new String[]{"Dragon" , "Wizard" , "Obsidian", "Amethyst" , "Grapes"}),

        GOLD(ChatColor.GOLD, DyeColor.YELLOW, new String[]{"Nemo" , "King" , "Pineapple", "Mango", "Champion"}),
        AQUA(ChatColor.AQUA, DyeColor.BLUE, new String[]{"Ice" , "Diamond" , "Dory", "Sky" , "Ice"}),
        LIME(ChatColor.GREEN, DyeColor.GREEN, new String[]{"Lime" , "Emerald" , "Kermat", "Turtle" , "Creeper"}),
        RED(ChatColor.RED, DyeColor.RED, new String[]{"Heart" , "Jam" , "Spiderman", "Tomato" , "Rose", "Apple", "Strawberry"}),
        PINK(ChatColor.LIGHT_PURPLE, DyeColor.PINK, new String[]{"Princess" , "Pig" , "Unicorn", "Barbie" , "Cake", "Love", "Flower", "Space"}),
        YELLOW(ChatColor.YELLOW, DyeColor.YELLOW, new String[]{"Banana" , "Minions" , "Sunshine", "Duck" , "Pikachu", "Bee"}),
        WHITE(ChatColor.WHITE, DyeColor.WHITE, new String[]{"Angel" , "Dove" , "Vanilla", "Snowman" , "Star"}),
        BLACK(ChatColor.BLACK, DyeColor.BLACK, new String[]{"Batman", "Orc" , "Agents", "Shadow"});

        private ChatColor color;
        private DyeColor dyeColor;
        private String[] names;

        TeamNames(ChatColor chatColor , DyeColor dyeColor, String[] names){
            this.color = chatColor;
            this.dyeColor = dyeColor;
            this.names = names;
        }

        public ChatColor getColor() {
            return color;
        }

        public DyeColor getDyeColor() {
            return dyeColor;
        }

        public String[] getNames() {
            return names;
        }
    }

}

/*

public enum UTeam {

    DARK_BLUE(ChatColor.DARK_BLUE, DyeColor.BLUE),
    DARK_GREEN(ChatColor.DARK_GREEN, DyeColor.GREEN),
    DARK_RED(ChatColor.DARK_RED, DyeColor.RED),
    DARK_PURPLE(ChatColor.DARK_PURPLE, DyeColor.PURPLE),

    GOLD(ChatColor.GOLD, DyeColor.ORANGE),
    BLUE(ChatColor.BLUE, DyeColor.LIGHT_BLUE),
    LIME(ChatColor.GREEN, DyeColor.LIME),
    AQUA(ChatColor.AQUA, DyeColor.CYAN),

    DARK_AQUA(ChatColor.DARK_AQUA, DyeColor.BROWN),
    RED(ChatColor.RED, DyeColor.PINK),
    PINK(ChatColor.LIGHT_PURPLE, DyeColor.MAGENTA),
    GRAY(ChatColor.GRAY, DyeColor.SILVER),
    DARK_GRAY(ChatColor.DARK_GRAY, DyeColor.GRAY),
    YELLOW(ChatColor.YELLOW, DyeColor.YELLOW),
    BLACK(ChatColor.BLACK, DyeColor.BLACK),
    WHITE(ChatColor.WHITE, DyeColor.WHITE),

    SOLO(ChatColor.YELLOW, null),
    SPECTATOR(ChatColor.GRAY, null);

    private String name;
    private ChatColor color;
    private DyeColor dyeColor;

    UTeam(String name, ChatColor color, DyeColor dyeColor) {
        this.name = name;
        this.color = color;
        this.dyeColor = dyeColor;
    }

    public DyeColor getDyeColor() {
        return dyeColor;
    }

    public ChatColor getColor() {
        return color;
    }

    @Override
    public String toString() {
        return super.toString().replace("_", " ");
    }
}
*/
