package net.vanillaverse.uhc.team;

import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.UPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class UTeamManager {

    public static HashMap<UTeam, Integer> uTeamKills = new HashMap<>();
    private static ArrayList<UTeam> teams = new ArrayList<>();

    public static UTeam players = new UTeam(ChatColor.YELLOW.toString() , ChatColor.YELLOW);
    public static UTeam spectators = new UTeam("Spec" , ChatColor.GRAY);

    public static void addTeamKill(UTeam team){
        uTeamKills.put(team, uTeamKills.get(team) + 1);
    }

    public static ArrayList<UTeam> getTeams(){
        return teams;
    }

    public static void generateTeams(){
        teams.clear();
        UTeam.TeamNames names = UTeam.TeamNames.DARK_GREEN;
        int counter = 0;
        for(int x = 1; x <= UGameManager.getSlots() / UGameManager.getTeamSizeLimit(); x++){
            names = getNextTeamName(names);
            if(x % UTeam.TeamNames.values().length == 0){
                counter++;
            }
            registerNewTeam(new UTeam(names.getNames()[counter], names.getColor()));
        }
    }

    public static void registerNewTeam(String name, ChatColor chatColor){
        UTeam team = new UTeam(name, chatColor);
        Bukkit.broadcastMessage("Registering team :" + team.getName());
        teams.add(team);
    }

    public static void registerNewTeam(UTeam team){
        teams.add(team);
    }

    public static UTeam.TeamNames getNextTeamName(UTeam.TeamNames names){
        for(UTeam.TeamNames name : UTeam.TeamNames.values()){
            if(name.ordinal() == names.ordinal() + 1)
                return name;
        }
        return UTeam.TeamNames.DARK_BLUE;
    }

    public static String[] getNames(ChatColor chatColor){
        for(UTeam.TeamNames names : UTeam.TeamNames.values()){
            if(names.getColor() == chatColor)
                return names.getNames();
        }
        return null;
    }

    public static boolean teamExists(String name){
        for(UTeam teams : getTeams()){
            if(teams.getName().equalsIgnoreCase(name))
                return true;
        }
        return false;
    }

    public static void switchTeam(UPlayer uPlayer, UTeam uTeam) {
        if (UGameManager.isTeamed()) {
            if (UGameManager.getTeamSizeLimit() <= UTeamManager.getTeamSize(uTeam) && isSoloOrSpec(uTeam)) {
                Chat.sendNoSpaceInTeam(uPlayer.getPlayer(), uTeam);
            } else {
                if (!isSoloOrSpec(uTeam))
                    Chat.switchTeam(uPlayer.getPlayer(), uTeam);
                    uPlayer.setTeam(uTeam);
            }
        } else {
            uPlayer.setTeam(players);

        }
    }

    public static UTeam getPlayersTeam(){
        return players;
    }

    public static UTeam getSpectators(){
        return spectators;
    }

    public static boolean isSoloOrSpec(UTeam team){
        if(team == spectators || team == players)
            return true;
        return false;
    }

    public static int getTeamKills(UTeam team){
        if(!uTeamKills.containsKey(team)){
            uTeamKills.put(team, 0);
        }
        return uTeamKills.get(team);
    }

    public static int getTeamSize(UTeam uTeam) {
        int count = 0;
        for (UPlayer uPlayer : UPlayer.getPlayers().values()) {
            if (uPlayer.getTeam() == uTeam) count++;
        }
        return count;
    }

    public static List<UPlayer> getTeamPlayers(UTeam uTeam) {
        return UPlayer.getPlayers().values().stream().filter(uPlayer -> uPlayer.getTeam() == uTeam).collect(Collectors.toCollection(ArrayList::new));
    }

}
