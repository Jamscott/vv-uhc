package net.vanillaverse.uhc.selector;

import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.game.UhcGameSettings;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.UPlayer;
import net.vanillaverse.uhc.team.TeamHologram;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import net.vanillaverse.uhc.util.Menus;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Colorable;
import org.bukkit.material.WoodenStep;
import org.bukkit.material.Wool;
import org.bukkit.scoreboard.Team;
import org.bukkit.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TeamSelector implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (UGameManager.getStage() == 1) {
            Block block = event.getPlayer().getLocation().add(0, -1, 0).getBlock();

            if (block != null && block.getType() == Material.STAINED_CLAY) {
                UPlayer uPlayer = UPlayer.getUPlayer(event.getPlayer());

                for(UTeam team : UTeamManager.getTeams()) {
                    if (UTeamManager.isSoloOrSpec(team)) continue;

                    if(UTeamManager.getTeamSize(team) == UGameManager.getTeamSizeLimit()) continue;

                    Location standLoc = TeamHologram.getTeamStand(team).getLocation();

                    if(event.getPlayer().getLocation().distance(standLoc) <= 3 && uPlayer.getTeam() != team){

                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1, 1);
                        UTeamManager.switchTeam(uPlayer, team);
                        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
                        LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
                        chestplateMeta.setColor(team.getDyeColor().getColor());
                        chestplate.setItemMeta(chestplateMeta);
                        event.getPlayer().getInventory().setChestplate(chestplate);

                        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
                        LeatherArmorMeta leggingsMeta = (LeatherArmorMeta) leggings.getItemMeta();
                        leggingsMeta.setColor(team.getDyeColor().getColor());
                        leggings.setItemMeta(leggingsMeta);
                        event.getPlayer().getInventory().setLeggings(leggings);

                        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                        LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();
                        bootsMeta.setColor(team.getDyeColor().getColor());
                        boots.setItemMeta(bootsMeta);
                        event.getPlayer().getInventory().setBoots(boots);

                    }
                }
            }
        }
    }
}
