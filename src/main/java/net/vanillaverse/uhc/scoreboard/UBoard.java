package net.vanillaverse.uhc.scoreboard;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jamm on 28/08/2016.
 */
public class UBoard {

    private static final BiMap<Integer, OfflinePlayer> playerHolder = HashBiMap.create(15);
    private final Scoreboard scoreboard;
    private final Objective objective;
    private final Map<Integer, Team> teams = new HashMap<>();
    private int counter;

    static {
        for (int i = 15; i >= 0; i--) {
            String name = ChatColor.values()[i].toString() + ChatColor.RESET.toString();
            playerHolder.put(i, Bukkit.getOfflinePlayer(name));
        }
    }

    public UBoard(Scoreboard scoreboard) {
        this.scoreboard = scoreboard;

        int teamCounter = 0;
        for (int i = 15; i >= 0; i--) {

            Team team = scoreboard.registerNewTeam("UHC_SS_" + teamCounter++);
            team.addPlayer(getPlayerForPosition(i));
            this.teams.put(i, team);
        }

        this.objective = scoreboard.registerNewObjective("OBJECTIVE", "dummy");
        this.objective.setDisplayName("VANILLAVERSE");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void clearText(int index) {
        setText(index, null);
    }

    public void setText(int index, String text) {

        int position = 15 - index;
        OfflinePlayer player = getPlayerForPosition(position);
        Team team = this.teams.get(position);

        if ((text == null) || (text.isEmpty())) {

            this.counter -= 1;
            team.setPrefix("");
            team.setSuffix("");
            this.scoreboard.resetScores(player);

            if (this.counter == 0) {

                this.scoreboard.clearSlot(DisplaySlot.SIDEBAR);

            }
            return;
        }
        this.counter += 1;

        if (this.scoreboard.getObjective(DisplaySlot.SIDEBAR) != this.objective) {
            this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        }

        applyTeamName(text, team);

        this.objective.getScore(player).setScore(position);
    }

    public void setTitle(String title) {
        this.objective.setDisplayName(title);
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public static OfflinePlayer getPlayerForPosition(Integer position) {
        return playerHolder.get(position);
    }

    public static Integer getPositionForPlayer(OfflinePlayer player) {
        return playerHolder.inverse().get(player);
    }

    private static void applyTeamName(String string, Team team) {

        if (string.length() <= 16) {

            team.setPrefix(string);
            team.setSuffix("");

        } else {

            String firstPart = string.substring(0, 16);
            String secondPart = ChatColor.getLastColors(firstPart) + string.substring(16, Math.min(32, string.length()));
            team.setPrefix(firstPart);
            team.setSuffix(secondPart.length() <= 16 ? secondPart : secondPart.substring(0, 16));

        }
    }

}
