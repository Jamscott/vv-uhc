package net.vanillaverse.uhc.scoreboard;

import net.vanillaverse.database.Database;
import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.game.UGameManager;
import net.vanillaverse.uhc.party.PartyManager;
import net.vanillaverse.uhc.player.Chat;
import net.vanillaverse.uhc.player.UPlayer;
import net.vanillaverse.uhc.team.UTeam;
import net.vanillaverse.uhc.team.UTeamManager;
import net.vanillaverse.uhc.tournament.RewardReveal;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;


public class UScoreManager {

    public static void init() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(UHC.INSTANCE, () ->  Bukkit.getOnlinePlayers().stream().forEach(UScoreManager::updatePlayer), 10L, 10);
    }

    public static void updatePlayer(Player player) {

        UPlayer uPlayer = UPlayer.getUPlayer(player);
        UBoard board = uPlayer.getScoreboard();
        Scoreboard scoreboard = board.getScoreboard();

        Objective health, health2;

        if(scoreboard.getObjective("hp-heart") == null)
            health = scoreboard.registerNewObjective("hp-heart", "health");
        else
            health = scoreboard.getObjective("hp-heart");

        if(scoreboard.getObjective("hp-heart2") == null)
            health2 = scoreboard.registerNewObjective("hp-heart2", "health2");
        else
            health2 = scoreboard.getObjective("hp-heart2");

        health.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        health2.setDisplayName(ChatColor.RED + "❤");
        health2.setDisplaySlot(DisplaySlot.BELOW_NAME);

        Bukkit.getOnlinePlayers().stream().forEach(p -> health.getScore(p.getName()).setScore((int) p.getHealth()));
        Bukkit.getOnlinePlayers().stream().forEach(p -> health2.getScore(p.getName()).setScore((int) p.getHealth()));

        int x = 0;

        board.setTitle(ChatColor.YELLOW + ChatColor.BOLD.toString() + "VanillaVerse UHC");

        if(UGameManager.getStage() == 1 || UGameManager.getStage() == 2) {

            board.setText(x++, " " + ChatColor.GOLD);
            board.setText(x++, "Players: " + ChatColor.GREEN + ChatColor.BOLD.toString() + Bukkit.getOnlinePlayers().size() + "/" + UGameManager.getSlots());
            board.setText(x++, "Mode: " + ChatColor.GREEN + Chat.BOLD +(UGameManager.isTeamed() ? "Teams of " + UGameManager.getTeamSizeLimit() : "Solo"));
            board.setText(x++, " " + ChatColor.GOLD);

            if (UGameManager.getStage() == 1) {

                if (UGameManager.getStartSec() > -1)
                    board.setText(x++, "Starting in " + ChatColor.GREEN + Chat.BOLD + UGameManager.getStartSec() + " seconds");
                else
                    board.setText(x++, "Waiting for players... ");

            } else {

                board.setText(x++, "Distributing in " + ChatColor.GREEN + ChatColor.BOLD + UGameManager.getSeconds() + " seconds");

            }
            board.setText(x++, "   ");


            if (UGameManager.isTeamed()) {

                if (UGameManager.isIsParties()) {

                    if (PartyManager.getParty(player) == null)
                        board.setText(x++, ChatColor.GREEN + ChatColor.BOLD.toString() + "/party to team");
                    else {

                        board.setText(x++, "Your party:");

                        if (PartyManager.getParty(player).getSize() == 1) {

                            board.setText(x++, "■ Invite friends!");

                        } else {

                            for (Player player1 : PartyManager.getParty(uPlayer.getPlayer()).getPlayers()) {
                                board.setText(x++, ChatColor.GREEN + "■ " + uPlayer.getTeam().getChatColor() + player1.getPlayer().getName());
                            }

                        }

                    }

                } else {

                    if (UTeamManager.isSoloOrSpec(uPlayer.getTeam())) {
                        board.setText(x++, ChatColor.GREEN + ChatColor.BOLD.toString() + "Join a team!");
                    } else {

                        board.setText(x++, "Your Team:");

                        for (UPlayer player1 : UTeamManager.getTeamPlayers(uPlayer.getTeam())) {
                            board.setText(x++, ChatColor.GREEN + "■ " + uPlayer.getTeam().getChatColor() + player1.getPlayer().getName());
                        }

                    }

                }

            } else {
                board.setText(x++, ChatColor.GREEN + Chat.BOLD + "Solo Match");
            }

            board.setText(x++, " ");
            board.setText(x++, ChatColor.YELLOW + "mc.vanillaverse.net");

        }else if(UGameManager.getStage() == 3){

            board.setText(x++, "" + ChatColor.GOLD);
            board.setText(x++, ChatColor.GOLD + "Rewards");

            for (int i = 1; i <= 3; i++) {

                if (RewardReveal.rewardRev >= i) {

                    board.setText(x++, RewardReveal.getRewardString(i));

                } else {

                    board.setText(x++ , ChatColor.GRAY + "" + ChatColor.MAGIC + "" + ChatColor.BOLD + "RewardsOP!" + i);

                }
            }

            board.setText(x++, " " + ChatColor.GOLD);


        }else if(UGameManager.getStage() == 4){

            board.setText(x++, "Players: " + ChatColor.GREEN + Chat.BOLD +Bukkit.getOnlinePlayers().size() + "/" + UGameManager.getSlots());
            board.setText(x++, " " + ChatColor.GOLD);
            board.setText(x++, ChatColor.WHITE + ChatColor.BOLD.toString() + "Distributing players...");
            board.setText(x++, " ");
            board.setText(x++, ChatColor.YELLOW + "mc.vanillaverse.net");

        }else if(UGameManager.getStage() == 5) {

            board.setText(x++, " " + ChatColor.GOLD);

            if (UGameManager.getSeconds() < 0) {

                board.setText(x++, "Starting in " + ChatColor.GREEN + Chat.BOLD + UGameManager.getSeconds() * -1 + " seconds");

            } else {

                String mins = (UGameManager.getSeconds() / 60) + "";
                if (mins.length() == 1) mins = "0" + mins;
                String secs = (UGameManager.getSeconds() % 60) + "";
                if (secs.length() == 1) secs = "0" + secs;
                board.setText(x++, "Time elapsed: " + ChatColor.GREEN + Chat.BOLD + mins + ":" + secs);

            }

            board.setText(x++, " ");

            if (uPlayer.getTeam() != UTeamManager.getSpectators()) {

                if (UGameManager.isTeamed()) {

                    board.setText(x++, "Team kills: " + ChatColor.GREEN + ChatColor.BOLD.toString() + UTeamManager.getTeamKills(UPlayer.getUPlayer(player).getTeam()));

                }

                board.setText(x++, "Your kills: " + ChatColor.GREEN + ChatColor.BOLD.toString() + UPlayer.getUPlayer(player).getKills());
                board.setText(x++, ChatColor.WHITE + " ");

            } else {
                board.setText(x++, " " + ChatColor.GOLD);
            }


            if (UGameManager.isTeamed() && uPlayer.getTeam() != UTeamManager.getSpectators()) {

                if(UTeamManager.getTeamSize(uPlayer.getTeam()) >= 2) {
                    board.setText(x++, ChatColor.GREEN + ChatColor.BOLD.toString() + "Your team members:");

                    int counter = 1;

                    for (UPlayer uPlayer1 : UTeamManager.getTeamPlayers(uPlayer.getTeam())) {

                        if (counter >= 4) break;
                        if (uPlayer == uPlayer1) continue;

                        boolean isNether = uPlayer.getPlayer().getWorld() == uPlayer1.getPlayer().getWorld();

                        String message = isNether ? (ChatColor.GREEN + ChatColor.BOLD.toString() + Chat.roundToDecimilPlace(player.getLocation().distance(uPlayer1.getPlayer().getLocation())) + "m ") : ChatColor.RED + ChatColor.BOLD.toString() + "Nether ";

                        board.setText(x++, message + ChatColor.WHITE + uPlayer1.getPlayer().getName());
                        counter++;
                    }
                    board.setText(x++, " ");
                }
            }


            board.setText(x++, "Border location:");
            board.setText(x++, ChatColor.GREEN + ChatColor.BOLD.toString() +  "-" + (int)Bukkit.getWorlds().get(0).getWorldBorder().getSize() +" to +" +  (int)Bukkit.getWorlds().get(0).getWorldBorder().getSize());
            board.setText(x++, ChatColor.YELLOW + "mc.vanillaverse.net");
        }

        for(Player p : Bukkit.getOnlinePlayers()){
            String teamName = UPlayer.getUPlayer(p).getTeam().getName() + p.getName();
            teamGen:{
                for (Team teams : board.getScoreboard().getTeams()) {
                    if (teams.getName().equalsIgnoreCase(teamName.length() > 16 ? teamName.substring(0, 15) : teamName)) {
                        break teamGen;
                    }
                }
                board.getScoreboard().registerNewTeam(teamName.length() > 16 ? teamName.substring(0, 15) : teamName);
            }
            String prefix = "";

            if(UTeamManager.isSoloOrSpec(UPlayer.getUPlayer(p).getTeam())){
                if(UPlayer.getUPlayer(p).getTeam() == UTeamManager.getPlayersTeam())
                    prefix = "";
                else
                    prefix = ChatColor.GRAY + ChatColor.BOLD.toString() + "SPEC " + ChatColor.GRAY;
            }

             prefix = UPlayer.getUPlayer(p).getTeam().getPrefix() + UPlayer.getUPlayer(p).getTeam().getChatColor();
            board.getScoreboard().getTeam(teamName.length() > 16 ? teamName.substring(0, 15) : teamName).addPlayer(p);
            board.getScoreboard().getTeam(teamName.length() > 16 ? teamName.substring(0, 15) : teamName).setPrefix(prefix);
        }

        do {
            board.clearText(x++);
        }
        while (x <= 15);

        if(player.getScoreboard() != board.getScoreboard())
            player.setScoreboard(board.getScoreboard());

        //UScoreboardTeamManager.updatePlayer(player, scoreboard);
    }

}
