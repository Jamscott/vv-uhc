package net.vanillaverse.uhc.scoreboard;

import net.vanillaverse.uhc.UHC;
import net.vanillaverse.uhc.player.UPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.AbstractMap;
import java.util.HashMap;

public class UScoreboardTeamManager {

    public static HashMap<String, AbstractMap.SimpleEntry<String, String>> entries = new HashMap<>();

    public static void init() {
        Bukkit.getScheduler().runTaskTimer(UHC.INSTANCE, UScoreboardTeamManager::updateEntries, 5L, 5L);
    }

    public static void updateEntries() {
        UPlayer.getPlayers().values().forEach(up -> {
            entries.put(up.getPlayer().getName(), new AbstractMap.SimpleEntry<>(up.getTeam().getChatColor() + ChatColor.BOLD.toString() + up.getTeam().getName() + up.getTeam().getChatColor(), ChatColor.RESET + ""));
        });
    }


    public static void updatePlayer(Player player, Scoreboard scoreboard) {
        for (String name : entries.keySet()) {
            AbstractMap.SimpleEntry<String, String> entry = entries.get(name);

            String teamName = name.length() > 15 ? entry.getKey() + name.substring(0, 15) : entry.getKey() + name;

            Team tt = scoreboard.registerNewTeam(teamName.length() >= 16 ? teamName.substring(0, 15) : teamName);
            if (entry.getKey() != null) tt.setPrefix(entry.getKey() != " " ? entry.getKey() + " " : entry.getKey());
            if (entry.getValue() != null) tt.setSuffix(entry.getValue());
            tt.addEntry(name);
        }

        player.setScoreboard(scoreboard);
    }
}
